<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="/assets/css/bootstrap.min.css" type="text/css" media="">
	<link rel="stylesheet" href="/assets/css/style.css" type="text/css" media="">
	<script type="text/javascript" src="/assets/js/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/assets/js/main.js"></script>
</head>
<body>
	<div class="container-fluid">
		<!-- Header -->
		<div class="header-wrapper col-xs-12 none-padding">
			<div class="header-content content-center">
				<div class="hd-content-left">
					<p>Hotline: 0934 111 373 | Email: info@anhthubeauty.com</p>
					<p>373 Trần Hưng Đạo, Cầu Kho, Quận 1, TP.HCM</p>
				</div>
				<div class="hd-content-right">
					<ul class="menu-header">
						<li>
							<a href="">Giới thiệu</a>
						</li>
						<li>
							<a href="">Khuyến mại</a>
						</li>
						<li>
							<a href="">Bác sĩ tư vấn</a>
						</li>
						<li>
							<a href="">Kiến thức làm đẹp</a>
						</li>
						<li>
							<a href="">Liên hệ</a>
						</li>
					</ul>
					<div class="lang-wrapper">
						<a href="">
							<span>
								US
							</span>
						</a>
						<a href="">
							<span class="active">
								Vietnam
							</span>
						</a>
					</div>
				</div>
				<div class="hd-banner">
					<a href="index.php">
						<img alt="Thẩm mỹ viện Anh Thư" rel="Thẩm mỹ viện Anh Thư" src="/assets/images/logo-header.png">
					</a>
				</div>
			</div>
		</div>

		<!-- Main menu -->
		<div class="main-menu-wrapper col-xs-12 none-padding">
			<div class="content-center">
				<ul class="main-menu-left">
					<li>
						<a href="">Trang chủ</a>
					</li>
					<li>
						<a href="">Giới thiệu</a>
					</li>
					<li>
						<a href="">Phẫu thuật thẩm mỹ</a>
					</li>
					<li>
						<a href="">Thẩm mỹ không phẫu thuật</a>
					</li>
				</ul>
				<ul class="main-menu-right main-menu-left">
					<li>
						<a href="">Điều trị và chăm sóc da</a>
						<ul>
							<li>
								<a href="">Chăm sóc da cơ bản</a>
							</li>
							<li>
								<a href="">Điều trị nám</a>
							</li>
							<li>
								<a href="">Điều trị mụn</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="">Bác sĩ tư vấn</a>
					</li>
					<li>
						<a href="">Gửi câu hỏi tư vấn 24/7</a>
					</li>
				</ul>
				<div class="logo-top">
					<a href="index.php">
						<img alt="Thẩm mỹ viện Anh Thư" rel="Thẩm mỹ viện Anh Thư" src="/assets/images/logo.png">
					</a>
				</div>
			</div>

		</div>
		<div class="contact-wrapper contact-category">
			<a class="contact-km" href=""></a>
			<a class="contact-tv" href=""></a>
			<a class="contact-facebook" href=""></a>
			<a class="contact-phone" href=""></a>
		</div>

		<!-- Head line -->
		<div class="head-line-wrapper col-xs-12 none-padding head-line-category">
			<div class="content-center head-line-content">
				<div class="social-wrapper">
					<a href="">
						<span class="facebook"></span>
					</a>
					<a href="">
						<span class="twitter"></span>
					</a>
					<a href="">
						<span class="youtube"></span>
					</a>
					<a href="">
						<span class="istagram"></span>
					</a>
				</div>
				<div class="hotline-wrapper">
					
				</div>
				<div class="search-wrapper">
					<form action="javascript:void(0);" method="post">
						<div class="input-wraaper">
							<input class="input-search" type="text" name="s" value="" />
							<input class="input-submit" type="submit" name="submit" value="" />
						</div>
					</form>
				</div>
			</div>
		</div>

		<!-- Content cat -->
		<div class="content-index-wrapper col-xs-12 none-padding category-content">
			<div class="content-center content-index">
				<div class="content-index-header cate-header">
					Điều trị và chăm sóc da
				</div>
				<div class="content-index-block">
					<div class="content-index-item cate-item">
						<a href="">
							<img src="/assets/images/1.jpg">
							<div class="content-item-title">
								Thẩm mỹ viện uy tín số 1 Việt Nam
							</div>
							<p>90 phút</p>
							<p>Công nghệ kép NANNOLIGHT & SKIKARMA xóa sạch vĩnh viễn những vết Nám da, đem lại một làn da trắng hồng không tỳ vết...</p>
							<a class="viewmore-cat" href="">Xem tiếp...</a>
						</a>
						
					</div>
					<div class="content-index-item cate-item">
						<a href="">
							<img src="/assets/images/2.jpg">
							<div class="content-item-title">
								Đội ngũ bác sĩ hàng đầu Việt Nam
							</div>
							<p>90 phút</p>
							<p>Công nghệ kép NANNOLIGHT & SKIKARMA xóa sạch vĩnh viễn những vết Nám da, đem lại một làn da trắng hồng không tỳ vết...</p>
							<a class="viewmore-cat" href="">Xem tiếp...</a>
						</a>
						
					</div>
					<div class="content-index-item cate-item">
						<a href="">
							<img src="/assets/images/3.jpg">
							<div class="content-item-title">
								Thiết bị hiện đại đạt chuẩn Châu Âu
							</div>
							<p>90 phút</p>
							<p>Công nghệ kép NANNOLIGHT & SKIKARMA xóa sạch vĩnh viễn những vết Nám da, đem lại một làn da trắng hồng không tỳ vết...</p>
							<a class="viewmore-cat" href="">Xem tiếp...</a>
						</a>
						
					</div>
					<div class="content-index-item cate-item">
						<a href="">
							<img src="/assets/images/4.jpg">
							<div class="content-item-title">
								Công nghệ thẩm mỹ hiện đại bậc nhất
							</div>
							<p>90 phút</p>
							<p>Công nghệ kép NANNOLIGHT & SKIKARMA xóa sạch vĩnh viễn những vết Nám da, đem lại một làn da trắng hồng không tỳ vết...</p>
							<a class="viewmore-cat" href="">Xem tiếp...</a>
						</a>
					</div>
					<div class="content-index-item cate-item">
						<a href="">
							<img src="/assets/images/4.jpg">
							<div class="content-item-title">
								Công nghệ thẩm mỹ hiện đại bậc nhất
							</div>
							<p>90 phút</p>
							<p>Công nghệ kép NANNOLIGHT & SKIKARMA xóa sạch vĩnh viễn những vết Nám da, đem lại một làn da trắng hồng không tỳ vết...</p>
							<a class="viewmore-cat" href="">Xem tiếp...</a>
						</a>
					</div>
					<div class="content-index-item cate-item">
						<a href="">
							<img src="/assets/images/4.jpg">
							<div class="content-item-title">
								Công nghệ thẩm mỹ hiện đại bậc nhất
							</div>
							<p>90 phút</p>
							<p>Công nghệ kép NANNOLIGHT & SKIKARMA xóa sạch vĩnh viễn những vết Nám da, đem lại một làn da trắng hồng không tỳ vết...</p>
							<a class="viewmore-cat" href="">Xem tiếp...</a>
						</a>
					</div>
					<div style="clear:both;"></div>
				</div>
			</div>
			<div style="clear:both;"></div>
		</div>

		<!-- Footer -->
		<div class="content-index-wrapper content-footer1-wrapper col-xs-12 none-padding">
			<div class="content-center content-index footer1-block">
				<div class="footer1-item">
					<div class="footer1-title">Giới thiệu</div>
					<ul>
						<li>
							<a href="">
								Đội ngũ bác sĩ, nhân viên
							</a>
						</li>
						<li>
							<a href="">
								Quy trình dịch vụ 
							</a>
						</li>
						<li>
							<a href="">
								Khuyến mãi
							</a>
						</li>
						<li>
							<a href="">
								Kiến thức làm đẹp
							</a>
						</li>
						<li>
							<a href="">
								Tuyển dụng
							</a>
						</li>
					</ul>
				</div>
				<div class="footer1-item">
					<div class="footer1-title">Phẫu thuật thẩm mỹ</div>
					<ul>
						<li>
							<a href="">
								Đội ngũ bác sĩ, nhân viên
							</a>
						</li>
						<li>
							<a href="">
								Quy trình dịch vụ 
							</a>
						</li>
						<li>
							<a href="">
								Khuyến mãi
							</a>
						</li>
						<li>
							<a href="">
								Kiến thức làm đẹp
							</a>
						</li>
						<li>
							<a href="">
								Tuyển dụng
							</a>
						</li>
					</ul>
				</div>
				<div class="footer1-item">
					<div class="footer1-title">Thẩm mỹ không phẫu thuật</div>
					<ul>
						<li>
							<a href="">
								Đội ngũ bác sĩ, nhân viên
							</a>
						</li>
						<li>
							<a href="">
								Quy trình dịch vụ 
							</a>
						</li>
						<li>
							<a href="">
								Khuyến mãi
							</a>
						</li>
						<li>
							<a href="">
								Kiến thức làm đẹp
							</a>
						</li>
						<li>
							<a href="">
								Tuyển dụng
							</a>
						</li>
					</ul>
				</div>
				<div class="footer1-item">
					<div class="footer1-title">Điều trị và chăm sóc da</div>
					<ul>
						<li>
							<a href="">
								Đội ngũ bác sĩ, nhân viên
							</a>
						</li>
						<li>
							<a href="">
								Quy trình dịch vụ 
							</a>
						</li>
						<li>
							<a href="">
								Khuyến mãi
							</a>
						</li>
						<li>
							<a href="">
								Kiến thức làm đẹp
							</a>
						</li>
						<li>
							<a href="">
								Tuyển dụng
							</a>
						</li>
					</ul>
				</div>

				<div class="map-wrapper">
					Google map
				</div>
				<a class="view-map" href="">Xem bản đồ lớn hơn</a>
			</div>
		</div>

		<!-- Footer 2 -->
		<div class="content-index-wrapper content-footer2-wrapper col-xs-12 none-padding">
			<div class="content-center content-index footer2-content">
				<div class="logo-footer">
					<a href="">
						<img src="/assets/images/logo-footer.png">
					</a>
				</div>
				<div class="fan-facebook">
					fanpage facebook
				</div>
				<div class="time-work">
					<p>Thời gian làm việc</p>
					<p>
						Buổi sáng:<span> 08:00 - 12:00</span>
					</p>
					<p>
						Buổi chiều:<span> 13:00 - 21:00</span>
					</p>
					<span>(Kể cả ngày lễ và chủ nhật)</span>
				</div>
				<div class="social-footer social-wrapper">
					<a href="">
						<span class="facebook"></span>
					</a>
					<a href="">
						<span class="twitter"></span>
					</a>
					<a href="">
						<span class="youtube"></span>
					</a>
					<a href="">
						<span class="istagram"></span>
					</a>
				</div>
				<div class="subscribe-wrapper">
					<form action="">
						<input class="text-input" type="text" placeholder="Email của bạn" />
						<input class="submit-input" type="submit" name="submit" value="Đăng ký nhận thông tin" />
					</form>
				</div>
				<div class="website-infor">
					<p>TMV Anh Thư | 373 Trần Hưng Đạo, Cầu Kho, Quận 1, TP.HCM</p>
					<p>Hotline: (08) 3920.2345 – (08) 3920.6789 – 0934111373 </p>
					<p>info@anhthubeauty.com | www.anhthubeauty.com</p>
				</div>
				<p class="copy-right">© Copyright 2015 anhthubeauty.com, All rights reserved.</p>
			</div>
		</div>
	</div>
</body>
</html>