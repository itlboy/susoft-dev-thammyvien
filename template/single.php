<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="/assets/css/bootstrap.min.css" type="text/css" media="">
	<link rel="stylesheet" href="/assets/css/style.css" type="text/css" media="">
	<script type="text/javascript" src="/assets/js/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/assets/js/main.js"></script>
</head>
<body>
	<div class="container-fluid">
		<!-- Header -->
		<div class="header-wrapper col-xs-12 none-padding">
			<div class="header-content content-center">
				<div class="hd-content-left">
					<p>Hotline: 0934 111 373 | Email: info@anhthubeauty.com</p>
					<p>373 Trần Hưng Đạo, Cầu Kho, Quận 1, TP.HCM</p>
				</div>
				<div class="hd-content-right">
					<ul class="menu-header">
						<li>
							<a href="">Giới thiệu</a>
						</li>
						<li>
							<a href="">Khuyến mại</a>
						</li>
						<li>
							<a href="">Bác sĩ tư vấn</a>
						</li>
						<li>
							<a href="">Kiến thức làm đẹp</a>
						</li>
						<li>
							<a href="">Liên hệ</a>
						</li>
					</ul>
					<div class="lang-wrapper">
						<a href="">
							<span>
								US
							</span>
						</a>
						<a href="">
							<span class="active">
								Vietnam
							</span>
						</a>
					</div>
				</div>
				<div class="hd-banner">
					<a href="index.php">
						<img alt="Thẩm mỹ viện Anh Thư" rel="Thẩm mỹ viện Anh Thư" src="/assets/images/logo-header.png">
					</a>
				</div>
			</div>
		</div>

		<!-- Main menu -->
		<div class="main-menu-wrapper col-xs-12 none-padding">
			<div class="content-center">
				<ul class="main-menu-left">
					<li>
						<a href="">Trang chủ</a>
					</li>
					<li>
						<a href="">Giới thiệu</a>
					</li>
					<li>
						<a href="">Phẫu thuật thẩm mỹ</a>
					</li>
					<li>
						<a href="">Thẩm mỹ không phẫu thuật</a>
					</li>
				</ul>
				<ul class="main-menu-right main-menu-left">
					<li>
						<a href="">Điều trị và chăm sóc da</a>
						<ul>
							<li>
								<a href="">Chăm sóc da cơ bản</a>
							</li>
							<li>
								<a href="">Điều trị nám</a>
							</li>
							<li>
								<a href="">Điều trị mụn</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="">Bác sĩ tư vấn</a>
					</li>
					<li>
						<a href="">Gửi câu hỏi tư vấn 24/7</a>
					</li>
				</ul>
				<div class="logo-top">
					<a href="index.php">
						<img alt="Thẩm mỹ viện Anh Thư" rel="Thẩm mỹ viện Anh Thư" src="/assets/images/logo.png">
					</a>
				</div>
			</div>

		</div>
		<div class="contact-wrapper contact-category">
			<a class="contact-km" href=""></a>
			<a class="contact-tv" href=""></a>
			<a class="contact-facebook" href=""></a>
			<a class="contact-phone" href=""></a>
		</div>

		<!-- Head line -->
		<div class="head-line-wrapper col-xs-12 none-padding head-line-category">
			<div class="content-center head-line-content">
				<div class="social-wrapper">
					<a href="">
						<span class="facebook"></span>
					</a>
					<a href="">
						<span class="twitter"></span>
					</a>
					<a href="">
						<span class="youtube"></span>
					</a>
					<a href="">
						<span class="istagram"></span>
					</a>
				</div>
				<div class="hotline-wrapper">
					
				</div>
				<div class="search-wrapper">
					<form action="javascript:void(0);" method="post">
						<div class="input-wraaper">
							<input class="input-search" type="text" name="s" value="" />
							<input class="input-submit" type="submit" name="submit" value="" />
						</div>
					</form>
				</div>
			</div>
		</div>

		<!-- Content cat -->
		<div class="content-index-wrapper col-xs-12 none-padding sg-content">
			<div class="content-center content-index">
				<div class="content-index-header cate-header">
					Chăm sóc da cơ bản
				</div>
				<div class="content-index-block">
				<!-- Menu single left -->
					<div class="menu-sg-left">
						<div class="menu-sg-header">
							Dịch vụ nổi bật
						</div>
						<ul class="list-art-sg">
							<li>
								<a href="">Nâng mũi S-Line bằng sụn tự thân</a>
							</li>
							<li>
								<a href="">Nâng mũi S-Line bằng sụn tự thân</a>
							</li>
							<li>
								<a href="">Triệt lông vĩnh viễn bằng NANNO LIGHT PRO</a>
							</li>
							<li>
								<a href="">Nâng mũi S-Line bằng sụn tự thân</a>
							</li>
						</ul>
						<div class="menu-sg-header">
							Phẫu thuật thẩm mỹ
						</div>
						<ul class="list-art-sg">
							<li>
								<a href="">Nâng mũi S-Line bằng sụn tự thân</a>
							</li>
							<li>
								<a href="">Nâng mũi S-Line bằng sụn tự thân</a>
							</li>
							<li>
								<a href="">Triệt lông vĩnh viễn bằng NANNO LIGHT PRO</a>
							</li>
							<li>
								<a href="">Nâng mũi S-Line bằng sụn tự thân</a>
							</li>
						</ul>

						<div class="menu-sg-header">
							Thẩm mỹ không phẫu thuật
						</div>
						<ul class="list-art-sg">
							<li>
								<a href="">Nâng mũi S-Line bằng sụn tự thân</a>
							</li>
							<li>
								<a href="">Nâng mũi S-Line bằng sụn tự thân</a>
							</li>
							<li>
								<a href="">Triệt lông vĩnh viễn bằng NANNO LIGHT PRO</a>
							</li>
							<li>
								<a href="">Nâng mũi S-Line bằng sụn tự thân</a>
							</li>
						</ul>

						<div class="menu-sg-header">
							Điều trị và chăm sóc da
						</div>
						<ul class="list-art-sg">
							<li>
								<a href="">Nâng mũi S-Line bằng sụn tự thân</a>
							</li>
							<li>
								<a href="">Nâng mũi S-Line bằng sụn tự thân</a>
							</li>
							<li>
								<a href="">Triệt lông vĩnh viễn bằng NANNO LIGHT PRO</a>
							</li>
							<li>
								<a href="">Nâng mũi S-Line bằng sụn tự thân</a>
							</li>
						</ul>
					</div>

					<!-- Content single -->
					<div class="content-sg-wrapper">
						<div class="sg-thmbnail">
							<img src="/assets/images/1.jpg">
						</div>
						<h1 class="art-sg-title">
							Những liệu trình chăm sóc da mặt định kỳ
						</h1>
						<p class="art-info">
							Thời gian: <span>30 phút</span>
						</p>
						<p class="art-info sg-price">
							Giá:  <span>400.000 - 1.200.000 VNĐ</span>
						</p>
						Dưới ảnh hưởng xấu và tác động tiêu cực từ môi trường, da sẽ dần bị tàn phai theo thời gian. Để duy trì làn da luôn khỏe mạnh, cùng với việc bổ sung đầy đủ nguồn dưỡng chất, ngủ nghỉ hợp lý thì việc chăm sóc da định kỳ cũng là việc làm hết sức thiết yếu.

						Những liệu trình chăm sóc da cơ bản tại TMV Anh Thư sẽ là giải pháp giúp bạn giữ gìn làn da chắc khỏe trẻ đẹp hoặc sẽ giúp bạn khôi phục làn da bị tổn thương, nâng cao độ đàn hồi cho da chắc khỏe.

						Liệu trình chăm sóc da bằng Collagen tươi

						Collage tươi giúp cải thiện độ đàn hồi và cải thiện cấu trúc da, làm cho da mượt và 			mịn hơn.
						Sau khi thẩm thấu vào các lớp bên trong của da, collagen tươi kích thích tái tạo tế 			bào, duy trì cân bằng nước và chống lại lão hoá trên da, làm mềm nếp nhăn và làm 			giảm nếp nhăn, cải thiện và nâng cao đường nét trên khuôn mặt.
						Collagen giải quyết hiệu quả làn da bị mất tính đàn hồi do chịu ảnh hưởng của thời 		gian và các yếu tố môi trường.
						Kích thích tái tạo tế bào, giúp trung hoà các tế bào gốc tự do, nuôi dưỡng các lớp tế 		bào sâu hơn của da.
						Collagen tươi còn có tác dụng chống nhăn, nâng và căng cơ mặt, chống lão hoá, ngăn ngừa nếp nhăn, dưỡng sáng mịn da và se nhỏ lỗ chân lông.
						<div class="contact-sg-wrapper">
							Hãy liên hệ ngay với các chuyên gia nhiệt tình của chúng tôi để bạn có được một buổi khám và tư vấn miễn phí! CALL: <span>0934.111.373</span>
						</div>
						<div class="sv-more-wrapper">
							<div class="sv-more-header">
								Các dịch vụ khác
							</div>
							<div class="sv-more-item">
								<a href="">
									<img src="/assets/images/1.jpg">
									<div class="sv-more-itemdes">
										Điều trị mụn
									</div>
								</a>
							</div>
							<div class="sv-more-item">
								<a href="">
									<img src="/assets/images/1.jpg">
									<div class="sv-more-itemdes">
										Điều trị mụn
									</div>
								</a>
							</div>
							<div class="sv-more-item">
								<a href="">
									<img src="/assets/images/1.jpg">
									<div class="sv-more-itemdes">
										Điều trị mụn
									</div>
								</a>
							</div>
							<div class="sv-more-item">
								<a href="">
									<img src="/assets/images/1.jpg">
									<div class="sv-more-itemdes">
										Điều trị mụn
									</div>
								</a>
							</div>
						</div>
						<div class="col-xs-12 none-padding dk-sv-single">
							<div class="dk-service">
								<div class="dk-service-header">
									
								</div>
								<form>
								  <div class="form-group">
								    <label for="exampleInputEmail1">Họ và tên</label>
								    <input type="email" class="form-control" placeholder="">
								  </div>
								  <div class="form-group">
								    <label for="exampleInputPassword1">Số điện thoại</label>
								    <input type="text" class="form-control" placeholder="">
								  </div>
								  <div class="form-group">
								    <label for="exampleInputPassword1">Email</label>
								    <input type="text" class="form-control" placeholder="">
								  </div>
								  <div class="form-group">
								    <label for="exampleInputPassword1">Ghi chú</label>
								    <textarea class="form-control"></textarea>
								  </div>
								  <div class="submit-wrapper">
								  		<button type="submit" class="btn btn-default">Đăng ký</button>
								  </div>
								</form>
							</div>
						</div>
						
					</div>
					<!-- Menu single right -->
					<div class="menu-sg-right">
						<div class="menu-sg-header">
							Bài viết mới
						</div>
						<div class="blog-list-wrapper">
							<ul>
								<li>
									<a href="">
										<img src="/assets/images/1.jpg">
										<div class="blog-list-item">
											Giảm béo mặt không còn là thách thức
										</div>
									</a>
								</li>
								<li>
									<a href="">
										<img src="/assets/images/1.jpg">
										<div class="blog-list-item">
											Giảm béo mặt không còn là thách thức
										</div>
									</a>
								</li>
								<li>
									<a href="">
										<img src="/assets/images/1.jpg">
										<div class="blog-list-item">
											Giảm béo mặt không còn là thách thức
										</div>
									</a>
								</li>
							</ul>
						</div>

						<div class="menu-sg-header">
							Bài viết nổi bật
						</div>
						<div class="blog-list-wrapper">
							<ul>
								<li>
									<a href="">
										<img src="/assets/images/1.jpg">
										<div class="blog-list-item">
											Giảm béo mặt không còn là thách thức
										</div>
									</a>
								</li>
								<li>
									<a href="">
										<img src="/assets/images/1.jpg">
										<div class="blog-list-item">
											Giảm béo mặt không còn là thách thức
										</div>
									</a>
								</li>
								<li>
									<a href="">
										<img src="/assets/images/1.jpg">
										<div class="blog-list-item">
											Giảm béo mặt không còn là thách thức
										</div>
									</a>
								</li>
							</ul>
						</div>

						<div class="menu-sg-header">
							Bác sĩ tư vấn
						</div>
						<div class="blog-list-wrapper">
							<ul>
								<li>
									<a href="">
										<img src="/assets/images/1.jpg">
										<div class="blog-list-item">
											Giảm béo mặt không còn là thách thức
										</div>
									</a>
								</li>
								<li>
									<a href="">
										<img src="/assets/images/1.jpg">
										<div class="blog-list-item">
											Giảm béo mặt không còn là thách thức
										</div>
									</a>
								</li>
								<li>
									<a href="">
										<img src="/assets/images/1.jpg">
										<div class="blog-list-item">
											Giảm béo mặt không còn là thách thức
										</div>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<div style="clear:both;"></div>
				</div>
			</div>
			<div style="clear:both;"></div>
		</div>

		<!-- Footer -->
		<div class="content-index-wrapper content-footer1-wrapper col-xs-12 none-padding">
			<div class="content-center content-index footer1-block">
				<div class="footer1-item">
					<div class="footer1-title">Giới thiệu</div>
					<ul>
						<li>
							<a href="">
								Đội ngũ bác sĩ, nhân viên
							</a>
						</li>
						<li>
							<a href="">
								Quy trình dịch vụ 
							</a>
						</li>
						<li>
							<a href="">
								Khuyến mãi
							</a>
						</li>
						<li>
							<a href="">
								Kiến thức làm đẹp
							</a>
						</li>
						<li>
							<a href="">
								Tuyển dụng
							</a>
						</li>
					</ul>
				</div>
				<div class="footer1-item">
					<div class="footer1-title">Phẫu thuật thẩm mỹ</div>
					<ul>
						<li>
							<a href="">
								Đội ngũ bác sĩ, nhân viên
							</a>
						</li>
						<li>
							<a href="">
								Quy trình dịch vụ 
							</a>
						</li>
						<li>
							<a href="">
								Khuyến mãi
							</a>
						</li>
						<li>
							<a href="">
								Kiến thức làm đẹp
							</a>
						</li>
						<li>
							<a href="">
								Tuyển dụng
							</a>
						</li>
					</ul>
				</div>
				<div class="footer1-item">
					<div class="footer1-title">Thẩm mỹ không phẫu thuật</div>
					<ul>
						<li>
							<a href="">
								Đội ngũ bác sĩ, nhân viên
							</a>
						</li>
						<li>
							<a href="">
								Quy trình dịch vụ 
							</a>
						</li>
						<li>
							<a href="">
								Khuyến mãi
							</a>
						</li>
						<li>
							<a href="">
								Kiến thức làm đẹp
							</a>
						</li>
						<li>
							<a href="">
								Tuyển dụng
							</a>
						</li>
					</ul>
				</div>
				<div class="footer1-item">
					<div class="footer1-title">Điều trị và chăm sóc da</div>
					<ul>
						<li>
							<a href="">
								Đội ngũ bác sĩ, nhân viên
							</a>
						</li>
						<li>
							<a href="">
								Quy trình dịch vụ 
							</a>
						</li>
						<li>
							<a href="">
								Khuyến mãi
							</a>
						</li>
						<li>
							<a href="">
								Kiến thức làm đẹp
							</a>
						</li>
						<li>
							<a href="">
								Tuyển dụng
							</a>
						</li>
					</ul>
				</div>

				<div class="map-wrapper">
					Google map
				</div>
				<a class="view-map" href="">Xem bản đồ lớn hơn</a>
			</div>
		</div>

		<!-- Footer 2 -->
		<div class="content-index-wrapper content-footer2-wrapper col-xs-12 none-padding">
			<div class="content-center content-index footer2-content">
				<div class="logo-footer">
					<a href="">
						<img src="/assets/images/logo-footer.png">
					</a>
				</div>
				<div class="fan-facebook">
					fanpage facebook
				</div>
				<div class="time-work">
					<p>Thời gian làm việc</p>
					<p>
						Buổi sáng:<span> 08:00 - 12:00</span>
					</p>
					<p>
						Buổi chiều:<span> 13:00 - 21:00</span>
					</p>
					<span>(Kể cả ngày lễ và chủ nhật)</span>
				</div>
				<div class="social-footer social-wrapper">
					<a href="">
						<span class="facebook"></span>
					</a>
					<a href="">
						<span class="twitter"></span>
					</a>
					<a href="">
						<span class="youtube"></span>
					</a>
					<a href="">
						<span class="istagram"></span>
					</a>
				</div>
				<div class="subscribe-wrapper">
					<form action="">
						<input class="text-input" type="text" placeholder="Email của bạn" />
						<input class="submit-input" type="submit" name="submit" value="Đăng ký nhận thông tin" />
					</form>
				</div>
				<div class="website-infor">
					<p>TMV Anh Thư | 373 Trần Hưng Đạo, Cầu Kho, Quận 1, TP.HCM</p>
					<p>Hotline: (08) 3920.2345 – (08) 3920.6789 – 0934111373 </p>
					<p>info@anhthubeauty.com | www.anhthubeauty.com</p>
				</div>
				<p class="copy-right">© Copyright 2015 anhthubeauty.com, All rights reserved.</p>
			</div>
		</div>
	</div>
</body>
</html>