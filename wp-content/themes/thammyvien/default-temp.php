<!-- Content cat -->
<div class="content-index-wrapper col-xs-12 none-padding sg-content">
    <div class="content-center content-index">
        <div class="content-index-header cate-header">
            <?php the_title() ?>
        </div>
        <div class="content-index-block">
            <div class="backsg-top">
            
            </div>
            <div class="backsg-bottom">
                
            </div>
            <?php get_template_part('sidebar', 'left') ?>
            <!-- Content single -->
            <?php echo $content ?>
            <?php get_template_part('sidebar', 'right') ?>
            <div style="clear:both;"></div>
        </div>
    </div>
    <div style="clear:both;"></div>
</div>