<?php
$args = array(
    'post_type' => 'dichvu',
    'post__not_in' => array(get_the_ID()),
    'posts_per_page' => 6, // Number of related posts that will be shown.
    'caller_get_posts' => 1
);
$otherSerices = get_posts($args);
?>
<div class="contact-sg-wrapper">
    Hãy liên hệ ngay với các chuyên gia nhiệt tình của chúng tôi để bạn có được một buổi khám và tư vấn miễn phí! CALL: <span>0934.111.373</span>
</div>
<?php if (has_tag()): ?>
    <div class="tags-sg-wrapper col-xs-12">
        <?php the_tags('Từ khóa: <br>'); ?>
    </div>
<?php endif; ?>
<div class="comment-form-wrapper col-xs-12 none-padding">
    <!--    <div class="comment-header">
    
        </div>-->
    <?php include('facebookComment.php') ?>
</div>
<?php if ($otherSerices): ?>
    <div class="sv-more-wrapper col-xs-12 none-padding">
        <div class="sv-more-header">
            Các dịch vụ khác
        </div>
        <?php
        global $post;
        foreach ($otherSerices as $post):
            ?>
            <div class="sv-more-item">
                <a href="<?php the_permalink() ?>">
                        <?php customThumb(193, 148) ?>
                    <div class="sv-more-itemdes">
        <?php the_title() ?>
                    </div>
                </a>
            </div>
    <?php endforeach; ?>
    </div>
<?php endif; ?>