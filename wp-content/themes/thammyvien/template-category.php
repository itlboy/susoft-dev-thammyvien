<?php
/*
 * Template Name: Danh mục
 * */
get_header()
?>
<!-- Content cat -->
<div class="content-index-wrapper col-xs-12 none-padding sg-content">
    <div class="content-center content-index">
        <div class="content-index-header cate-header">
            Chăm sóc da cơ bản
        </div>
        <div class="content-index-block">
            <!-- Menu single left -->
            <div class="menu-sg-left">
                <div class="menu-sg-header">
                    Dịch vụ nổi bật
                </div>
                <ul class="list-art-sg">
                    <li>
                        <a href="">Nâng mũi S-Line bằng sụn tự thân</a>
                    </li>
                    <li>
                        <a href="">Nâng mũi S-Line bằng sụn tự thân</a>
                    </li>
                    <li>
                        <a href="">Triệt lông vĩnh viễn bằng NANNO LIGHT PRO</a>
                    </li>
                    <li>
                        <a href="">Nâng mũi S-Line bằng sụn tự thân</a>
                    </li>
                </ul>
                <div class="menu-sg-header">
                    Phẫu thuật thẩm mỹ
                </div>
                <ul class="list-art-sg">
                    <li>
                        <a href="">Nâng mũi S-Line bằng sụn tự thân</a>
                    </li>
                    <li>
                        <a href="">Nâng mũi S-Line bằng sụn tự thân</a>
                    </li>
                    <li>
                        <a href="">Triệt lông vĩnh viễn bằng NANNO LIGHT PRO</a>
                    </li>
                    <li>
                        <a href="">Nâng mũi S-Line bằng sụn tự thân</a>
                    </li>
                </ul>

                <div class="menu-sg-header">
                    Thẩm mỹ không phẫu thuật
                </div>
                <ul class="list-art-sg">
                    <li>
                        <a href="">Nâng mũi S-Line bằng sụn tự thân</a>
                    </li>
                    <li>
                        <a href="">Nâng mũi S-Line bằng sụn tự thân</a>
                    </li>
                    <li>
                        <a href="">Triệt lông vĩnh viễn bằng NANNO LIGHT PRO</a>
                    </li>
                    <li>
                        <a href="">Nâng mũi S-Line bằng sụn tự thân</a>
                    </li>
                </ul>

                <div class="menu-sg-header">
                    Điều trị và chăm sóc da
                </div>
                <ul class="list-art-sg">
                    <li>
                        <a href="">Nâng mũi S-Line bằng sụn tự thân</a>
                    </li>
                    <li>
                        <a href="">Nâng mũi S-Line bằng sụn tự thân</a>
                    </li>
                    <li>
                        <a href="">Triệt lông vĩnh viễn bằng NANNO LIGHT PRO</a>
                    </li>
                    <li>
                        <a href="">Nâng mũi S-Line bằng sụn tự thân</a>
                    </li>
                </ul>
            </div>

            <!-- Content single -->
            <div class="content-sg-wrapper content-catblog">
                <div class="cat-item">
                    <a class="cat-item-thumbnail" href="">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/1.jpg">
                    </a>
                    <div class="cat-item-info">
                        <a href="">
                            Giảm béo mặt không còn là thách thức
                        </a>
                        <div class="cat-item-des">
                            Giảm béo mặt không còn là thách thức Giảm béo mặt không còn là thách thức
                            Giảm béo mặt không còn là thách thức
                        </div>
                    </div>
                </div>

                <div class="cat-item">
                    <a class="cat-item-thumbnail" href="">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/1.jpg">
                    </a>
                    <div class="cat-item-info">
                        <a href="">
                            Giảm béo mặt không còn là thách thức
                        </a>
                        <div class="cat-item-des">
                            Giảm béo mặt không còn là thách thức Giảm béo mặt không còn là thách thức
                            Giảm béo mặt không còn là thách thức
                        </div>
                    </div>
                </div>
                <div class="panigation-wrapper">
                    <ul>
                        <li>
                            <a href=""><<<</a>
                        </li>
                        <li>
                            <a href=""><</a>
                        </li>
                        <li>
                            <a href="">1</a>
                        </li>
                        <li>
                            <a href="">..</a>
                        </li>
                        <li>
                            <a href="">10</a>
                        </li>
                        <li>
                            <a href="">></a>
                        </li>
                        <li>
                            <a href="">>>></a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Menu single right -->
            <div class="menu-sg-right">
                <div class="menu-sg-header">
                    Bài viết mới
                </div>
                <div class="blog-list-wrapper">
                    <ul>
                        <li>
                            <a href="">
                                <img src="<?php echo get_template_directory_uri() ?>/assets/images/1.jpg">
                                <div class="blog-list-item">
                                    Giảm béo mặt không còn là thách thức
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <img src="<?php echo get_template_directory_uri() ?>/assets/images/1.jpg">
                                <div class="blog-list-item">
                                    Giảm béo mặt không còn là thách thức
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <img src="<?php echo get_template_directory_uri() ?>/assets/images/1.jpg">
                                <div class="blog-list-item">
                                    Giảm béo mặt không còn là thách thức
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="menu-sg-header">
                    Bài viết nổi bật
                </div>
                <div class="blog-list-wrapper">
                    <ul>
                        <li>
                            <a href="">
                                <img src="<?php echo get_template_directory_uri() ?>/assets/images/1.jpg">
                                <div class="blog-list-item">
                                    Giảm béo mặt không còn là thách thức
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <img src="<?php echo get_template_directory_uri() ?>/assets/images/1.jpg">
                                <div class="blog-list-item">
                                    Giảm béo mặt không còn là thách thức
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <img src="<?php echo get_template_directory_uri() ?>/assets/images/1.jpg">
                                <div class="blog-list-item">
                                    Giảm béo mặt không còn là thách thức
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="menu-sg-header">
                    Bác sĩ tư vấn
                </div>
                <div class="blog-list-wrapper">
                    <ul>
                        <li>
                            <a href="">
                                <img src="<?php echo get_template_directory_uri() ?>/assets/images/1.jpg">
                                <div class="blog-list-item">
                                    Giảm béo mặt không còn là thách thức
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <img src="<?php echo get_template_directory_uri() ?>/assets/images/1.jpg">
                                <div class="blog-list-item">
                                    Giảm béo mặt không còn là thách thức
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <img src="<?php echo get_template_directory_uri() ?>/assets/images/1.jpg">
                                <div class="blog-list-item">
                                    Giảm béo mặt không còn là thách thức
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div style="clear:both;"></div>
        </div>
    </div>
    <div style="clear:both;"></div>
</div>
<?php get_footer() ?>