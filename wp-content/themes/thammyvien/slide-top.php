<?php
$numberSlide = count(get_field('banners', 'option'));
?>
<!-- Banner top -->
<!--<div class="banner-top-wraper col-xs-12 none-padding">
    <div class="content-center slide-wrapper">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
             Indicators 
            <ol class="carousel-indicators carousel-bannertop">
<?php for ($i = 1; $i <= $numberSlide; $i++): ?>
                                    <li data-target="#carousel-example-generic" data-slide-to="<?php echo ($i - 1) ?>" class="active"><?php echo $i ?></li>
<?php endfor; ?>
            </ol>
            <div class="carousel-inner" role="listbox">
<?php
$active = 'active';
if (have_rows('banners', 'option')):
    while (have_rows('banners', 'option')): the_row();
	?>

							<div class="item <?php echo $active ?>">
							    <img src="<?php the_sub_field('image'); ?>">
							</div>

	<?php
	if ($active)
	    $active = "";
    endwhile;
    ?>
<?php endif; ?>
            </div>
        </div>
    </div>
    
</div>-->
<div class="banner-top-wraper col-xs-12 none-padding">
    <div class="content-center slide-wrapper">
	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
	    <?php putRevSlider("fp-slider") ?>
	</div>
    </div>
</div>
<div class="contact-wrapper content-destop">
    <a class="contact-km" href="<?php echo get_category_link(16) ?>"></a>
    <a class="contact-tv" href=""  data-toggle="modal" data-target="#myModal"></a>
    <a class="contact-facebook" href="<?php the_field('facebook_fanpage', 'option'); ?>"></a>
    <a class="contact-phone" href=""></a>
</div>