<?php
/*
 * Template Name: Giới thiệu
 * */
$images = get_field('top_slide');
$hello = get_field('hello');
$customers = get_field('customers');
$doctorImage = get_field('doctor_image');
$imageUrl = $images[0]['image']['url'];
?>
<?php get_header() ?>
<?php //get_template_part('template-gioithieu', 'content')                    ?>
<!-- Content cat -->
<div class="content-index-wrapper col-xs-12 none-padding sg-content">
    <div class="content-center content-index">
        <div class="content-index-header cate-header">
            <?php the_title() ?>
        </div>
        <div class="content-index-block">
            <?php get_template_part('sidebar', 'left') ?>
            <!-- Content single -->
            <div class="content-sg-wrapper sg-page-wrapper">
                <div class="slide-page-wrapper col-xs-12 none-padding">
                    <div id="carousel-sgpage-generic" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators carousel-bannertop">
                            <?php
                            for ($i = 1; $i <= count($images); $i++):
                                if ($i == 1) {
                                    $active = "active";
                                } else {
                                    $active = '';
                                }
                                ?>
                                <li data-target="#carousel-sgpage-generic" data-slide-to="<?php echo ($i - 1) ?>" class="<?php echo $active ?>"><?php echo $i; ?></li>
                            <?php endfor; ?>
                        </ol>
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <?php
                            $first = true;
                            foreach ($images as $image):
                                if ($first) {
                                    $active = "active";
                                    $first = false;
                                } else {
                                    $active = '';
                                }
                                ?>
                                <div class="item <?php echo $active ?>">
                                    <img src="<?php echo $image['image']['url'] ?>">
                                </div>
                                <!--                                <div class="item">
                                                                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/banner.png" alt="...">
                                                                </div>-->
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <h1 class="art-sg-title">
                    <?php echo $hello; ?>
                </h1>
                <?php the_content(); ?>
                <div class="sg-page-testi">
                    <h2>
                        Khách hàng của chúng tôi
                    </h2>
                    <?php
                    foreach ($customers as $customer):
                        if ($customer['percent'] > 88) {

                            $lightClass = "";
                            $boldClass = "bold-progress";
                        } else {
                            $lightClass = "light-progress";
                            $boldClass = "";
                        }
                        ?>
                        <div class="progress <?php echo $lightClass ?>">
                            <div class="progress-bar <?php echo $boldClass ?>" role="progressbar" aria-valuenow="<?php echo $customer['percent'] ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $customer['percent'] ?>%;">
                                <?php echo $customer['customer_type'] ?> <?php echo $customer['percent'] ?>%
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <div class="dt-wrapper">
                        <img src="<?php echo $doctorImage ?>">
                        <?php the_field('doctor_description') ?>
<!--                        <h2>Tiến sĩ - Bác sĩ Nguyễn Anh Tuấn</h2>
                        <p class="caption">Trưởng khoa Tạo Hình Thẩm Mỹ</p>
                        <p class="caption">Bệnh Viện Đại Học Y Dược TP. Hồ Chí Minh</p>
                        <div class="dt-content">
                            Với gần 30 năm kinh nghiệm, trong đó 20 năm là phẫu thuật viên Chấn thương Chỉnh hình và gần 10 năm là phẫu thuật viên thẩm mỹ, bác sĩ Nguyễn Anh Tuấn từng theo học chuyên sâu ngành Tạo hình – Thẩm mỹ tại các nước tiên tiến như Đức, Hoa Kỳ… và được mệnh danh là đôi bàn tay vàng trong ngành Tạo hình – Thẩm mỹ Việt Nam với những ca phẫu thuật an toàn, hiệu quả làm đẹp rõ rệt và mang lại sự hài lòng cho khách hàng. Tại AnhthuBeauty, Tiến sĩ – bác sĩ Nguyễn Anh Tuấn đảm nhận vai trò phẫu thuật viên chính cho tất cả các ca phẫu thuật thẩm mỹ. Phương châm của ông là mang lại vẻ đẹp tự nhiên, hài hòa và phù hợp nhất với cá tính riêng của mỗi khách hàng, giúp họ tự tin và hạnh phúc hơn với vẻ bề ngoài của mình.
                        </div>-->
                    </div>

                </div>
                <?php // get_template_part('content', 'bottom')   ?>
                <?php // get_template_part('content', 'register')  ?>
            </div>
            <?php get_template_part('sidebar', 'right') ?>
            <div style="clear:both;"></div>
        </div>
    </div>
    <div style="clear:both;"></div>
</div>
<?php get_footer() ?>