<?php
$promotionCategoryId = 16;
$numberPost = 4;
$args = array(
    'posts_per_page' => $numberPost,
    'category' => $promotionCategoryId,
);
$promtionPosts = get_posts($args);
$helpCategoryId = 17;
$args = array(
    'posts_per_page' => $numberPost,
    'category' => $helpCategoryId,
);
$helpPosts = get_posts($args);
?>

<!-- Khuyen mai -->
<div class="content-index-wrapper content-km-wrapper col-xs-12 none-padding">
    <div class="content-center content-index">
        <div class="km-block col-xs-12 none-padding">
            <div class="block-header">
                <a href="<?php echo get_category_link($promotionCategoryId) ?>"> Khuyến mại</a>
            </div>
            <?php
            global $post;
            $post = $promtionPosts[0];
            setup_postdata($post);
            ?>
            <a href="<?php the_permalink() ?>">
                <?php customThumb(320,240) ?>
                <div class="art-title">
                    <?php the_title() ?>
                </div>
            </a>
            <div class="art-des">
                <?php echo excerpt(50) ?>
            </div>
            <ul class="art-list">
                <?php
                for ($i = 1; $i <= 3; $i++):
                    if (isset($promtionPosts[$i])) {
                        $post = $promtionPosts[$i];
                        setup_postdata($post);
                    } else {
                        break;
                    }
                    ?>
                    <li>
                        <a href="<?php the_permalink() ?>">
                            <?php the_title() ?>
                        </a>
                    </li>
                <?php endfor; ?>
            </ul>
            <a class="view-more" href="<?php echo get_category_link($promotionCategoryId) ?>">
                Xem tiếp...
            </a>
        </div>
        <div class="km-block tv-block col-xs-12 none-padding">
            <div class="block-header">
                <a href="<?php echo get_category_link($helpCategoryId) ?>">Tư vấn dịch vụ</a>
            </div>
            <?php
            global $post;
            $post = $helpPosts[0];
            setup_postdata($post);
            ?>
            <a href="<?php the_permalink() ?>">
                <?php customThumb(320,240) ?>
                <div class="art-title">
                    <?php the_title() ?>
                </div>
            </a>
            <div class="art-des">
                 <?php echo excerpt(50) ?>
            </div>
            <ul class="art-list">
                <?php
                for ($i = 1; $i <= 3; $i++):
                    if (isset($helpPosts[$i])) {
                        $post = $helpPosts[$i];
                        setup_postdata($post);
                    } else {
                        break;
                    }
                    ?>
                    <li>
                        <a href="<?php the_permalink() ?>">
                            <?php the_title() ?>
                        </a>
                    </li>
                <?php endfor; ?>
            </ul>
            <a class="view-more" href="<?php echo get_category_link($helpCategoryId) ?>">
                Xem tiếp...
            </a>
        </div>
        <div class="dk-service-wrapper col-md-4 col-xs-12 none-padding">
            <div class="dk-service">
                <div class="dk-service-header">

                </div>
                <?php include 'registerForm.php' ?>
            </div>
        </div>
    </div>
</div>
<?php
wp_reset_postdata();
?>