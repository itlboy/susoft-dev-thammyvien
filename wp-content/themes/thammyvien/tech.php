<?php
$categoryId = 14;
$numberPost = 4;
$args = array(
    'posts_per_page' => $numberPost,
    'category' => $categoryId,
);
$posts = get_posts($args);
$videoLink = get_field('video_link', 'option')
?> 
<!-- Cong nghe noi bat -->
<div class="content-index-wrapper content-tech-wrapper col-xs-12 none-padding">
    <div class="content-center content-index">
        <a href="<?php echo get_category_link($categoryId) ?>">
            <div class="sv-index-header ">
                <?php echo get_the_category_by_ID($categoryId) ?>
            </div>
        </a>
        <div class="content-index-block content-tech-index content-destop">
            <div class="tech-item-block">
                <?php
                global $post;
                foreach ($posts as $post):
                    setup_postdata($post);
                    ?>
                    <div class="content-index-item">
                        <a href="<?php the_permalink() ?>">
                            <?php customThumb(198, 198); ?>
                            <?php // the_post_thumbnail(); ?>
                            <div class="content-item-title">
                                <?php the_title() ?>
                            </div>
                        </a>
                        <div class="tect-item-des">
                            <?php the_excerpt(); ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <!-- video clip -->
            <div class="video-wrapper">
                <div class="video-header">
                    <span class="video-icon"></span>
                    Video clip
                </div>
                <div class="video-player">
                    <iframe width="100%" height="270" src="<?php echo $videoLink ?>" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <!-- Album anh -->
            <div class="album-wrapper video-wrapper">
                <div class="video-header">
                    <span class="album-icon"></span>
                    Album ảnh
                </div>
                <div class="album-block">
                    <?php
                    if (have_rows('images', 'option')):
                        while (have_rows('images', 'option')): the_row();
                            ?>
                            <div class="album-item">
                                <a href="">
                                    <?php customThumb(210, 168, get_sub_field('image')) ?>
                                    <!--<img src="<?php // get_sub_field('image'); ?>">-->
                                    <div class="album-item-des">
                                        <?php the_sub_field('description'); ?>
                                    </div>
                                </a>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="content-index-block content-tech-mobile content-sv-index content-techmobile content-mobile">
            <div id="carousel-techmobile-generic" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner col-xs-12 none-padding" role="listbox">
                    <div class="item active">
                        <div class="item-wrapper">
                        <?php
                        global $post;
                        $i = 0;
                        foreach ($posts as $post):
                            $i++;
                            setup_postdata($post);
                            ?>
                            <div class="content-index-item">
                                <a href="<?php the_permalink() ?>">
                                    <?php customThumb(198, 198); ?>
                                    <?php // the_post_thumbnail(); ?>
                                    <div class="content-item-title">
                                        <?php the_title() ?>
                                    </div>
                                </a>
                                <div class="tect-item-des">
                                    <?php the_excerpt(); ?>
                                </div>
                            </div>
                            <?php
                            if ($i == 2)
                                break;
                        endforeach;
                        ?>
                        </div>
                    </div>
                    <div class="item">
                        <div class="item-wrapper">
                        <?php
                        global $post;
                        for ($i = 2; $i > 0; $i++):
                            if (!isset($posts[$i]))
                                break;
                            else {
                                $post = $posts[$i];
                                setup_postdata($post);
                            }
                            ?>
                            <div class="content-index-item">
                                <a href="<?php the_permalink() ?>">
                                    <?php customThumb(198, 198); ?>
                                    <?php // the_post_thumbnail(); ?>
                                    <div class="content-item-title">
                                        <?php the_title() ?>
                                    </div>
                                </a>
                                <div class="tect-item-des">
                                    <?php the_excerpt(); ?>
                                </div>
                            </div>
                            <?php
                        endfor;
                        ?>
                        </div>
                    </div>
                </div>
                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-techmobile-generic" role="button" data-slide="prev">
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-techmobile-generic" role="button" data-slide="next">
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <div style="clear:both;"></div>
    </div>
</div>
<?php wp_reset_postdata(); ?>