<?php require_once 'preProcess.php'; ?>
<!DOCTYPE html>
<html>
    <head>
	<?php wp_head(); ?>
        <title><?php pageTitle() ?></title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/bootstrap.min.css" type="text/css" media="">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/style.css" type="text/css" media="">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/custom.css" type="text/css" media="">
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/assets/js/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/assets/js/main.js"></script>
        <!--<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/assets/js/jquery.form.min.js"></script>-->
        <meta property="fb:admins" content="1015482144"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    </head>
    <body>
        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5&appId=375771025895677";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
        <!-- Banner left & right -->
        <div class="banner-left">
	    <?php if (get_field('banner_left_on', 'option') && get_field('banner_left', 'option')):
		?>
    	    <img src="<?php the_field('banner_left', 'option'); ?>" />
	    <?php endif; ?>
        </div>
        <div class="banner-right">
	    <?php if (get_field('banner_right_on', 'option') && get_field('banner_right', 'option')):
		?>
    	    <img src="<?php the_field('banner_right', 'option'); ?>" />
	    <?php endif; ?>
        </div>
        <div class="slide-menu-wrapper">
            <div class="close-menu">X</div>
            <div class="search-wrapper  col-xs-12 none-padding">
                <form method="GET" action="/">
                    <div class="input-wraaper">
                        <input type="text" value="" name="s" class="input-search">
                        <input type="submit" value="" name="submit" class="input-submit">
                    </div>
                </form>
            </div>
            <div class="social-footer social-wrapper  col-xs-12 none-padding">
                <a href="https://www.facebook.com/TMVanhthu">
                    <span class="facebook"></span>
                </a>
                <a href="">
                    <span class="twitter"></span>
                </a>
                <a href="">
                    <span class="youtube"></span>
                </a>
                <a href="">
                    <span class="istagram"></span>
                </a>
            </div>
            <div class="col-xs-12 none-padding menu-item-wrapper">
                <!-- <div class="menu-sg-header ">
                    <a href="http://thammyvien.local/danhmuc-dichvu/dich-vu-noi-bat/">
                        Trang chủ        
                    </a>
                </div> -->
        		<?php
        		wp_nav_menu(array(
        		    'theme_location' => 'headerLeft',
        		    'menu_class' => '',
        		    'container' => '',
        		));
        		?>
        		<?php
        		wp_nav_menu(array(
        		    'theme_location' => 'headerRight',
        		    'menu_class' => '',
        		    'container' => '',
        		));
        		?>
            </div>
            <div class="logo-footer content-mobile col-xs-12">
                <a href="<?php echo bloginfo('home') ?>">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/logo-footer.png">
                </a>
            </div>
            <div class="website-infor col-xs-12 content-mobile">
                <p>TMV Anh Thư | 373 Trần Hưng Đạo, P. Cầu Kho, Quận 1, TP.HCM</p>
                <p>Hotline: (08) 3920.2345 – (08) 3920.6789 – 0934111373 </p>
                <p>info@anhthubeauty.com | www.anhthubeauty.com</p>
            </div>
        </div>
        <div class="container-fluid">
            <!-- Header -->
            <div class="header-wrapper col-xs-12 none-padding content-destop">
                <div class="header-content content-center">
                    <div class="hd-content-left">
                        <p>Hotline: 0934 111 373 | Email: info@anhthubeauty.com</p>
                        <p>373 Trần Hưng Đạo, P. Cầu Kho, Quận 1, TP.HCM</p>
                    </div>
                    <div class="hd-content-right">
			<?php
			wp_nav_menu(array(
			    'theme_location' => 'topMenu',
			    'menu_class' => 'menu-header',
			    'container' => '',
			));
			?>
                        <div class="lang-wrapper">
<!--                            <div id="google_translate_element"></div><script type="text/javascript">
                                function googleTranslateElementInit() {
                                    new google.translate.TranslateElement({pageLanguage: 'vi', includedLanguages: 'en', autoDisplay: false, multilanguagePage: true}, 'google_translate_element');
                                }
                            </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>-->
                            <a href="">
                                <span>
                                    US
                                </span>
                            </a>
                            <a href="">
                                <span class="active">
                                    Vietnam
                                </span>
                            </a>
                        </div>
                    </div>
                    <div class="hd-banner">
                        <a href="index.php">
                            <img alt="Thẩm mỹ viện Anh Thư" rel="Thẩm mỹ viện Anh Thư" src="<?php echo get_template_directory_uri() ?>/assets/images/logo-header.png">
                        </a>
                    </div>
                </div>
            </div>
            <!-- Main menu -->
            <div class="main-menu-wrapper col-xs-12 none-padding content-destop">
                <div class="content-center">
		    <?php
		    wp_nav_menu(array(
			'theme_location' => 'headerLeft',
			'menu_class' => 'main-menu-left',
			'container' => '',
		    ));
		    ?>
		    <?php
		    wp_nav_menu(array(
			'theme_location' => 'headerRight',
			'menu_class' => 'main-menu-right main-menu-left',
			'container' => '',
		    ));
		    ?>
                    <div class="logo-top">
                        <a href="index.php">
                            <img alt="Thẩm mỹ viện Anh Thư" rel="Thẩm mỹ viện Anh Thư" src="<?php echo get_template_directory_uri() ?>/assets/images/logo.png">
                        </a>
                    </div>
                </div>

            </div>
            <!-- Header mobile -->
            <div class="col-xs-12 none-padding header-mb-wrapper content-mobile">
                <div class="col-xs-3 none-padding menu-button">

                </div>
                <div class="col-xs-1 none-padding logo-mb">
                    <a href="<?php bloginfo('name'); ?>">

                    </a>
                </div>
                <div class="col-xs-3 none-padding logo-hd-mb">
                </div>
            </div>
	    <?php
	    if (is_home())
		get_template_part('slide', 'top');
	    ?>
            <!-- Head line -->
            <div class="head-line-wrapper col-xs-12 none-padding">
                <div class="content-center head-line-content">
                    <div class="social-wrapper content-destop">
                        <a href="<?php the_field('facebook_fanpage', 'option'); ?>">
                            <span class="facebook"></span>
                        </a>
                        <a href="<?php the_field('twitter_link', 'option'); ?>">
                            <span class="twitter"></span>
                        </a>
                        <a href="<?php the_field('youtube_link', 'option'); ?>">
                            <span class="youtube"></span>
                        </a>
                        <a href="<?php the_field('istagram_link', 'option'); ?>">
                            <span class="istagram"></span>
                        </a>
                    </div>
                    <div class="hotline-wrapper">
                        <span>Hotline tư vấn: </span>0934 111 373
                    </div>
                    <div class="search-wrapper content-destop">
                        <form action="/" method="GET">
                            <div class="input-wraaper">
                                <input class="input-search" type="text" name="s" value="" />
                                <input class="input-submit" type="submit" name="submit" value="" />
                            </div>
                        </form>
                    </div>
                </div>
            </div>