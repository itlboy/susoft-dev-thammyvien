<?php
$categoryId = 15;
$numberPost = 6;
$args = array(
    'posts_per_page' => $numberPost,
    'category' => $categoryId,
);
$posts = get_posts($args);
$videoLink = get_field('video_link', 'option')
?> 
<!-- Khach hang noi ve TMV -->
<div class="content-index-wrapper content-testi-wrapper col-xs-12 none-padding">
    <div class="content-center content-index">
        <div class="sv-index-header ">
        </div>
        <div class="content-index-block content-sv-index content-testi-index content-destop">
            <div id="carousel-testi-generic" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <?php
                        global $post;
                        for ($i = 0; $i <= 2; $i++):
                            if (isset($posts[$i])) {
                                $post = $posts[$i];
                            } else {
                                break;
                            }
                            setup_postdata($post);
                            ?>
                            <div class="content-index-item">
                                <a href="<?php the_permalink() ?>">
                                    <?php customThumb(294, 214) ?>
                                    <div class="content-item-title">
                                        <?php the_title() ?>
                                    </div>
                                    <div class="testi-des">
                                        <?php the_excerpt() ?>
                                    </div>
                                </a>
                            </div>
                        <?php endfor; ?>
                    </div>
                    <div class="item">
                        <?php
                        global $post;
                        for ($i = 3; $i > 0; $i++):
                            if (isset($posts[$i])) {
                                $post = $posts[$i];
                            } else {
                                break;
                            }
                            setup_postdata($post);
                            ?>
                            <div class="content-index-item">
                                <a href="<?php the_permalink() ?>">
                                  <?php customThumb(294, 214) ?>
                                    <div class="content-item-title">
                                        <?php the_title() ?>
                                    </div>
                                    <div class="testi-des">
                                        <?php the_excerpt() ?>
                                    </div>
                                </a>
                            </div>
                        <?php endfor; ?>
                    </div>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-testi-generic" role="button" data-slide="prev">
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-testi-generic" role="button" data-slide="next">
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <div class="content-index-block content-sv-index content-testi-index content-mobile">
            <div id="carousel-testimobile-generic" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <div class="item-wrapper">
                            <?php
                            global $post;
                            for ($i = 0; $i < 2; $i++):
                                if (isset($posts[$i])) {
                                    $post = $posts[$i];
                                } else {
                                    break;
                                }
                                setup_postdata($post);
                                ?>
                                <div class="content-index-item">
                                    <a href="<?php the_permalink() ?>">
                                        <?php customThumb(294, 214) ?>
                                        <div class="content-item-title">
                                            <?php the_title() ?>
                                        </div>
                                        <div class="testi-des">
                                            <?php the_excerpt() ?>
                                        </div>
                                    </a>
                                </div>
                            <?php endfor; ?>
                        </div>
                    </div>
                    <div class="item">
                        <div class="item-wrapper">
                        <?php
                        global $post;
                        for ($i = 2; $i < 4; $i++):
                            if (isset($posts[$i])) {
                                $post = $posts[$i];
                            } else {
                                break;
                            }
                            setup_postdata($post);
                            ?>
                            <div class="content-index-item">
                                <a href="<?php the_permalink() ?>">
                                  <?php customThumb(294, 214) ?>
                                    <div class="content-item-title">
                                        <?php the_title() ?>
                                    </div>
                                    <div class="testi-des">
                                        <?php the_excerpt() ?>
                                    </div>
                                </a>
                            </div>
                        <?php endfor; ?>
                        </div>
                    </div>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-testimobile-generic" role="button" data-slide="prev">
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-testimobile-generic" role="button" data-slide="next">
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</div>