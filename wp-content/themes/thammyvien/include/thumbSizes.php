<?php

add_action('after_setup_theme', 'mytheme_custom_thumbnail_size');

function mytheme_custom_thumbnail_size() {
    add_image_size('sidebar', 110, 110, true);
    add_image_size('category', 200, 180, true);
    add_image_size('home', 198, 198, true);
    add_image_size('home-big', 450, 360, true);
    add_image_size('home-slide', 294, 214, true);
}
