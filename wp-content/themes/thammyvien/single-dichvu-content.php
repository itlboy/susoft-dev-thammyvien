<?php
$timeToService = get_field('time');
$priceFrom = get_field('price_from');
$priceTo = get_field('price_to');
?>
<!-- Content cat -->
<div class="content-index-wrapper col-xs-12 none-padding sg-content">
    <div class="content-center content-index">
        <div class="content-index-header cate-header">
            <?php the_title() ?>
        </div>
        <div class="content-index-block">
            <div class="backsg-top">

            </div>
            <div class="backsg-bottom">

            </div>
            <?php get_template_part('sidebar', 'left') ?>
            <!-- Content single -->
            <div class="content-sg-wrapper">
                <?php if (has_post_thumbnail()): ?>
                    <div class="sg-thmbnail">
                        <?php customThumb(293,215); ?>
                    </div>
                <?php endif; ?>
                <h1 class = "art-sg-title">
                    <?php the_title()
                    ?>
                </h1>
                <?php if ($timeToService): ?>
                    <p class="art-info">
                        Thời gian: <span><?php echo $timeToService ?> phút</span>
                    </p>
                <?php endif; ?>
                <?php if ($priceFrom || $priceTo): ?>
                    <p class="art-info sg-price">
                        Giá:  <span>
                            <?php if ($priceFrom): ?>
                                <?php echo number_format($priceFrom) . ' - ' ?>
                            <?php endif; ?>
                            <?php if ($priceTo): ?>
                                <?php echo number_format($priceTo) . ' VNĐ ' ?>
                            <?php endif; ?>
                            <!--400.000 - 1.200.000 VNĐ-->
                        </span>
                    </p>
                <?php endif; ?>
                <?php
                if (have_posts()) {
                    the_post();
                    the_content();
                }
                ?>
                <?php get_template_part('content', 'bottom') ?>
                <?php get_template_part('content', 'register') ?>
            </div>
            <?php get_template_part('sidebar', 'right') ?>
            <div style="clear:both;"></div>
        </div>
    </div>
    <div style="clear:both;"></div>
</div>