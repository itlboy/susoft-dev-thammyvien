<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Subscriber
 *
 * @author Kien
 */
class Subscriber {

    //put your code here
    public static function run() {
	self::insertJs();
	self::processRequest();
    }

    public static function insertJs() {
	add_action('wp_footer', 'sub_action_javascript');

	function sub_action_javascript() {
	    ?><script>// <![CDATA[
	                    jQuery(document).ready(function () {
	                        if (typeof $ == 'undefined')
	                            var $ = jQuery;
	                        $("#registerform").on("submit", function (event) {
	                            event.preventDefault();
	                            var email = $('#registerform .text-input').val()
	                            if (typeof email == 'undefined' || email.length < 3) {
	                                alert("Xin vui lòng nhập vào email")
	                                return false;
	                            }
	                            $.ajax({
	                                url: '<?php echo admin_url("admin-ajax.php"); ?>',
	                                method: 'POST',
	                                data: {action: 'sub_action', email: email},
	                                success: function (str) {
	                                    alert('Đã đăng ký thành công');
	                                    $('#registerform .text-input').val('')
	                                }
	                            })
	                        });
	                    });
	                    // ]]></script>
	    <?php
	}

    }

    public static function processRequest() {
	add_action('wp_ajax_sub_action', 'sub_action');
	add_action('wp_ajax_nopriv_sub_action', 'sub_action');

	function sub_action() {
	    $email = $_REQUEST['email'];
	    $postArr = array(
		'post_title' => $email,
		'post_type' => 'dangkynhanthongtin',
		'post_status' => 'publish',
	    );
	    $id = wp_insert_post($postArr, true);
	    die();
	}

    }

}
