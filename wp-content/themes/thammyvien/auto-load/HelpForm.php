<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HelpForm
 *
 * @author Kien
 */
class HelpForm {

    //put your code here
    public static function run() {
	self::insertJs();
	self::processRequest();
    }

    public static function insertJs() {
	add_action('wp_footer', 'dvd_action_javascript');

	function dvd_action_javascript() {
	    ?><script>// <![CDATA[
	                    jQuery(document).ready(function () {
	                        if (typeof $ == 'undefined')
	                            var $ = jQuery;
	                        $('.main-menu-wrapper a').click(function (e) {
	                            if ($(this).attr('title') == 'modal') {
	                                e.preventDefault();
	                                $('#myModal').modal('toggle');
	                            }
	                        })

	                        $(".help-form").on("submit", function (event) {
	                            event.preventDefault();
	                            var id = $(event.currentTarget).attr('id');
	                            var name = $('#' + id + ' .name').val();
	                            var mobile = $('#' + id + ' .mobile').val();
	                            var email = $('#' + id + ' .email').val();
	                            var note = $('#' + id + ' .note').val();
	                            if (mobile.length == 0 && email.length == 0) {
	                                alert('Xin vui lòng nhập vào email hoặc số điện thoại');
	                                return;
	                            }
	                            if (note.length == 0) {
	                                alert('Xin vui lòng nhập nội dung cần tư vấn');
	                                return;
	                            }
	                            $.ajax({
	                                url: '<?php echo admin_url("admin-ajax.php"); ?>',
	                                method: 'POST',
	                                data: {name: name, mobile: mobile, email: email,
	                                    note: note, action: 'register_action'},
	                                success: function (str) {
	                                    if (id == 'help-form2') {
	                                        $('#myModal').modal('toggle');
	                                    }
	                                    $('#' + id).find("input[type=text], textarea").val("");
	                                    alert('Cảm ơn bạn đã gửi câu hỏi, chúng tôi sẽ phản hồi sớm nhất');
	    //                                console.log(str);
	                                }
	                            })
	                        });
	                    });
	                    // ]]></script>
	    <?php
	}

    }

    public static function processRequest() {
	add_action('wp_ajax_register_action', 'register_action');
	add_action('wp_ajax_nopriv_register_action', 'register_action');

	function register_action() {
	    $name = $_REQUEST['name'];
	    $email = $_REQUEST['email'];
	    $mobile = $_REQUEST['mobile'];
	    $note = $_REQUEST['note'];
	    $postArr = array(
		'post_title' => $name . '/' . $email . '/' . $mobile,
		'post_content' => $note,
		'post_type' => 'cauhoituvan',
		'post_status' => 'publish',
	    );
	    $id = wp_insert_post($postArr, true);
//            update_field($field_key, $value, $id);
	    die();
	}

    }

}
?>
