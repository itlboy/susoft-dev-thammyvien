jQuery(document).ready(function () {
    function setSize(elm, height) {
        var widthScreen = jQuery(window).width() / 1170,
                css = {
                    "zoom": 1, /* IE */
                    "-moz-transform": "scale(" + widthScreen + ")", /* Firefox */
                    "-moz-transform-origin": "0 0",
                    "-o-transform": "scale(" + widthScreen + ")", /* Opera */
                    "-o-transform-origin": "0 0",
                    "-webkit-transform": "scale(" + widthScreen + ")", /* Safari And Chrome */
                    "-webkit-transform-origin": "0 0",
                    "transform": "scale(" + widthScreen + ")", /* Standard Property */
                    "transform-origin": "0 0",
                    "height": height,
                    // "float":"left",
                    // "overflow":"scroll"
                };
        jQuery(elm).css(css);
    }
    function setSizeWindow() {
        var widthScreen = jQuery(window).width() / 1170,
                css = {
                    "zoom": 1, /* IE */
                    "-moz-transform": "scale(" + widthScreen + ")", /* Firefox */
                    "-moz-transform-origin": "0 0",
                    "-o-transform": "scale(" + widthScreen + ")", /* Opera */
                    "-o-transform-origin": "0 0",
                    "-webkit-transform": "scale(" + widthScreen + ")", /* Safari And Chrome */
                    "-webkit-transform-origin": "0 0",
                    "transform": "scale(" + widthScreen + ")", /* Standard Property */
                    "transform-origin": "0 0",
                    "height": jQuery(window).height() + "px",
                    "float": "left",
                    "overflow": "scroll"
                };
        jQuery("body").css(css);
    }
    if (jQuery(window).width() <= 1024) {
        // setSize(jQuery("#carousel-example-generic"),'185px');
        // setSize(jQuery(".head-line-content"),'118px');
        // setSize(jQuery(".sv-index-header"),'65px');

    }
    if (jQuery(window).width() >= 1550) {
        // setSizeWindow();
    }
    jQuery(window).resize(function () {
        if (jQuery(window).width() <= 650) {
            // setSize();
            // setSizeWindow();
        }
    });

    //Slide menu
    jQuery(".slide-menu-wrapper").height(jQuery(window).height());
    jQuery(".menu-button").click(function () {
        if (jQuery(".slide-menu-wrapper.active").length != 0) {
            jQuery(".slide-menu-wrapper").fadeOut();
            jQuery("body").css("margin-left", "0");
            jQuery(".slide-menu-wrapper").removeClass("active");
        } else {
            // jQuery("body").css("margin-left",jQuery(".slide-menu-wrapper").width()+"px");
            jQuery(".slide-menu-wrapper").fadeIn();
            jQuery(".slide-menu-wrapper").addClass("active");
        }

    });
    jQuery(".close-menu").click(function () {
        jQuery(".slide-menu-wrapper").fadeOut();
        jQuery("body").css("margin-left", "0");
        jQuery(".slide-menu-wrapper").removeClass("active");
    });

    //Banner top
    jQuery(window).scroll(function () {
        var position = jQuery(this).scrollTop();
        if (position > 600) {
            jQuery(".banner-left").fadeIn();
            jQuery(".banner-right").fadeIn();
        } else {
            jQuery(".banner-left").fadeOut();
            jQuery(".banner-right").fadeOut();
        }

    });

    //colapse footer
    jQuery(".content-footer1-wrapper .collapse").on('show.bs.collapse', function (e) {
        // do something…
        jQuery(e.currentTarget).siblings(".in").collapse('hide');
//        jQuery.each(, function( index, value ) {
//            jQuery(value).collapse('hide');
//        });
//        console.log(jQuery(e.currentTarget).siblings(".collapse"));
    })
});