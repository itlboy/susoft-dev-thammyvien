<?php
get_header();
$args = array(
    'post_type' => 'bacsituvan',
    'post__not_in' => array(get_the_ID()),
    'posts_per_page' => 6, // Number of related posts that will be shown.
    'caller_get_posts' => 1
);
$relatePosts = get_posts($args);
?>
<div class="content-index-wrapper col-xs-12 none-padding sg-content">
    <div class="content-center content-index">
        <div class="content-index-header cate-header">
            <?php the_title() ?>
        </div>
        <div class="content-index-block">
            <div class="backsg-top">

            </div>
            <div class="backsg-bottom">

            </div>
            <?php get_template_part('sidebar', 'left') ?>
            <!-- Content single -->
            <div class="content-sg-wrapper">
                <h1 class="faq-title">
                    <?php the_title(); ?>
                </h1>
                <div class="faq-content col-xs-12 none-padding">
                    <?php if (has_post_thumbnail()): ?>
                        <?php the_post_thumbnail(); ?>
                    <?php endif; ?>
                    <?php the_field('question') ?>
                </div>
                <p class="faq-source"><?php the_field('address') ?></p>
                <div class="answer-wrapper col-xs-12 none-padding">
                    <p class="answer">Trả lời</p>
                    <?php the_content(); ?>
                    <p class="faq-source">Bác sĩ tư vấn</p>
                    <p class="faq-source">anhthubeauty.com</p>
                </div>
                <div class="contact-sg-wrapper">
                    Hãy liên hệ ngay với các chuyên gia nhiệt tình của chúng tôi để bạn có được một buổi khám và tư vấn miễn phí! CALL: <span>0934.111.373</span>
                </div>
                <?php
                if (count($relatePosts) > 0):
                    global $post;
                    ?>
                    <div class="post-related-wrapper col-xs-12 none-padding">
                        <div class="post-related-header">
                            Các tin liên quan
                        </div>
                        <ul>
                            <?php
                            foreach ($relatePosts as $post):
                                setup_postdata($post);
                                ?>
                                <li>
                                    <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
                                </li>
                                <?php ?>
                            </ul>
                        </div>
                        <?php
                    endforeach;
                endif;
                ?>
                <?php // get_template_part('content', 'bottom')  ?>
                <?php get_template_part('register') ?>
            </div>
            <!-- Menu single right -->
            <?php get_template_part('sidebar', 'right'); ?>
            <div style="clear:both;"></div>
        </div>
    </div>
    <div style="clear:both;"></div>
</div>
<?php get_footer(); ?>