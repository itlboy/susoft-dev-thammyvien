<?php
global $leftSideBarTerms;
//var_dump($leftSideBarTerms);
//die();
?>
<div class="backsg-top">

</div>
<div class="backsg-bottom">

</div>
<!-- Menu single left -->
<div class="menu-sg-left">
    <?php
//for a given post type, return all
    if ($leftSideBarTerms) {
        foreach ($leftSideBarTerms as $tax_term) {
            wp_reset_query();
//            $tax_term = get_term($termId, $tax);
//            $posts_array = get_posts(
            $args = array(
                'posts_per_page' => -1,
                'post_type' => SERVICEPOSTTYPE,
                'post_status' => 'publish',
                'tax_query' => array(
                    array(
                        'taxonomy' => SERVICETAX,
                        'field' => 'term_id',
                        'terms' => $tax_term->term_id,
                        'include_children' => false
                    )
                )
            );
            $my_query = null;
            $my_query = new WP_Query($args);
            ?>
            <div class="menu-sg-header">
                <a href="<?php echo get_term_link($tax_term) ?>" >
                    <?php echo $tax_term->name; ?>
                </a>
            </div>
            <?php
            if ($my_query->have_posts()):
                ?>
                <ul class="list-art-sg">
                    <?php
                    while ($my_query->have_posts()) : $my_query->the_post();
                        ?>
                        <li>
                            <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                        </li>
                        <?php
                    endwhile;
                    ?>
                </ul>
                <?php
            endif;
            wp_reset_query();
        }
    }
    ?>
</div>
