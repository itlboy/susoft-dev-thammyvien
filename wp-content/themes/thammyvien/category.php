<?php
get_header()
?>
<!-- Content cat -->
<div class="content-index-wrapper col-xs-12 none-padding sg-content">
    <div class="content-center content-index">
        <div class="content-index-header cate-header">
            <?php
            if (get_post_type() == 'bacsituvan') {
                echo 'Bác sĩ tư vấn';
            } else {
                if (is_tag())
                    echo '#';
                single_cat_title('', true);
            }
            ?>
        </div>
        <div class="content-index-block custom-content" >
            <!-- Menu single left -->
            <?php get_template_part('sidebar', 'left') ?>
            <!-- Content single -->
            <div class="content-sg-wrapper content-catblog">
                <?php
                if (have_posts()):
                    while (have_posts()):
                        the_post();
                        ?>
                        <div class="cat-item">
                            <a class="cat-item-thumbnail" href="<?php the_permalink() ?>">
                                <?php the_post_thumbnail('category'); ?>
                            </a>
                            <div class="cat-item-info">
                                <a href="<?php the_permalink() ?>">
                                    <?php the_title() ?>
                                </a>
                                <div class="cat-item-des">
                                    <?php echo excerpt(70); ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    endwhile;
                endif;
                ?>
                <?php page_nav() ?>
            </div>
            <!-- Menu single right -->

            <?php get_template_part('sidebar', 'right') ?>
            <div style="clear:both;"></div>
        </div>
    </div>
    <div style="clear:both;"></div>
</div>
<?php get_footer() ?>