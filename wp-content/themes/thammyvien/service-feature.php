<?php
$categoryId = 13;
$numberPost = 8;
$args = array(
    'posts_per_page' => $numberPost,
    'category' => $categoryId,
);
$posts = get_posts($args);
?> 
<!-- Dich vu noi bat -->
<div class="content-index-wrapper sv-index-wrapper col-xs-12 none-padding">
    <div class="content-center content-index">
        <a href="<?php echo get_category_link($categoryId) ?>">
            <div class="sv-index-header ">
                <?php echo get_the_category_by_ID($categoryId) ?>
            </div>
        </a>
        <div class="content-index-block content-sv-index content-destop">
            <div id="carousel-sv-generic" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <?php
                        global $post;
                        $i = 0;
                        foreach ($posts as $post):
                            $i++;
                            setup_postdata($post);
                            ?>
                            <div class="content-index-item">
                                <a href="<?php the_permalink() ?>">
                                    <?php customThumb(198, 198); ?>
                                    <?php // the_post_thumbnail() ?>
                                    <div class="content-item-title">
                                        <?php the_title() ?>
                                    </div>
                                </a>
                            </div>
                            <?php
                            if ($i == 4)
                                break;
                        endforeach;
                        ?>
                    </div>
                    <div class="item">
                        <?php
                        global $post;
                        for ($i = 4; $i > 0; $i++):
                            if (!isset($posts[$i]))
                                break;
                            else {
                                $post = $posts[$i];
                                setup_postdata($post);
                            }
                            ?>
                            <div class="content-index-item">
                                <a href="<?php the_permalink() ?>">
                                    <?php customThumb(198, 198); ?>
                                    <?php // the_post_thumbnail() ?>
                                    <div class="content-item-title">
                                        <?php the_title() ?>
                                    </div>
                                </a>
                            </div>
                            <?php
                        endfor;
                        ?>
                    </div>
                </div>
                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-sv-generic" role="button" data-slide="prev">
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-sv-generic" role="button" data-slide="next">
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <div class="content-index-block content-sv-index content-sv-mobile content-mobile">
            <div id="carousel-sv-generic" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <?php
                        global $post;
                        $i = 0;
                        foreach ($posts as $post):
                            $i++;
                            setup_postdata($post);
                            ?>
                            <div class="content-index-item">
                                <a href="<?php the_permalink() ?>">
                                    <?php customThumb(198, 198); ?>
                                    <?php // the_post_thumbnail() ?>
                                    <div class="content-item-title">
                                        <?php the_title() ?>
                                    </div>
                                </a>
                            </div>
                            <?php
                            if ($i == 2)
                                break;
                        endforeach;
                        ?>
                    </div>
                    <div class="item">
                        <?php
                        global $post;
                        for ($i = 2; $i > 0; $i++):
                            if (!isset($posts[$i]))
                                break;
                            else {
                                $post = $posts[$i];
                                setup_postdata($post);
                            }
                            ?>
                            <div class="content-index-item">
                                <a href="<?php the_permalink() ?>">
                                    <?php customThumb(198, 198); ?>
                                    <?php // the_post_thumbnail() ?>
                                    <div class="content-item-title">
                                        <?php the_title() ?>
                                    </div>
                                </a>
                            </div>
                            <?php
                            if ($i == 3)
                                break;
                        endfor;
                        ?>
                    </div>
                </div>
                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-sv-generic" role="button" data-slide="prev">
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-sv-generic" role="button" data-slide="next">
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</div>
<?php wp_reset_postdata(); ?>