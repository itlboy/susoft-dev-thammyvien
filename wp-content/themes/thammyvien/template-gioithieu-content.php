<?php
$images = get_field('top_slide');
$imageUrl = $images[0]['image']['url'];
?>
<!-- Content cat -->
<div class="content-index-wrapper col-xs-12 none-padding sg-content">
    <div class="content-center content-index">
        <div class="content-index-header cate-header">
            <?php the_title() ?>
        </div>
        <div class="content-index-block">
            <?php get_template_part('sidebar', 'left') ?>
            <!-- Content single -->
            <div class="content-sg-wrapper">
                <div class="sg-thmbnail">
                    <img src="<?php echo $imageUrl ?>" />
                    <?php // the_post_thumbnail() ?>
                </div>
                <?php while (have_posts()) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
                    <?php the_content(); ?> <!-- Page Content -->
                    <?php
                endwhile; //resetting the page loop
                wp_reset_query(); //resetting the page query
                ?>
                <?php get_template_part('content', 'bottom') ?>
                <?php get_template_part('content', 'register') ?>
            </div>
            <?php get_template_part('sidebar', 'right') ?>
            <div style="clear:both;"></div>
        </div>
    </div>
    <div style="clear:both;"></div>
</div>