<?php
global $featurePosts, $newPosts, $supportPosts;
?>

<!-- Menu single right -->
<div class="menu-sg-right">
    <?php if ($newPosts): ?>
        <div class="menu-sg-header">
            <a href="<?php echo get_archive_t ?>">
                Tiêu điểm
            </a>
        </div>
        <div class="blog-list-wrapper">
            <ul>
                <?php
                global $post;
                foreach ($newPosts as $post):
                    setup_postdata($post);
                    ?>
                    <li>
                        <a href="<?php the_permalink(); ?>">
                            <?php the_post_thumbnail('sidebar') ?>
                            <div class="blog-list-item">
                                <?php the_title(); ?>
                            </div>
                        </a>
                    </li>
                    <?php
                endforeach;
                wp_reset_postdata();
                ?>
            </ul>
        </div>
    <?php endif; ?>
    <?php if ($featurePosts): ?>
        <div class="menu-sg-header">
            Công nghệ nổi bật
        </div>
        <div class="blog-list-wrapper">
            <ul>
                <?php
                global $post;
                foreach ($featurePosts as $post):
                    setup_postdata($post);
                    ?>
                    <li>
                        <a href="<?php the_permalink(); ?>">
                            <?php the_post_thumbnail('sidebar') ?>
                            <div class="blog-list-item">
                                <?php the_title(); ?>
                            </div>
                        </a>
                    </li>
                    <?php
                endforeach;
                wp_reset_postdata();
                ?>
            </ul>
        </div>
    <?php endif; ?>
    <?php if ($supportPosts):
        ?>
        <div class="menu-sg-header">
            <a href="<?php echo get_post_type_archive_link('bacsituvan') ?>">
                Bác sĩ tư vấn
            </a>
        </div>
        <div class="blog-list-wrapper">
            <ul>
                <?php
                global $post;
                foreach ($supportPosts as $post):
                    setup_postdata($post);
                    ?>
                    <li>
                        <a href="<?php the_permalink(); ?>">
                            <?php the_post_thumbnail('sidebar') ?>
                            <div class="blog-list-item">
                                <?php the_title(); ?>
                            </div>
                        </a>
                    </li>
                    <?php
                endforeach;
                wp_reset_postdata();
                ?>
            </ul>
        </div>
    <?php endif; ?>
</div>
<?php wp_reset_query() ?>