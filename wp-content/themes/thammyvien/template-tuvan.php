<?php
/*
 * Template Name: Bác sĩ tư vấn
 * */
get_header();
?>
<div class="content-index-wrapper col-xs-12 none-padding sg-content">
    <div class="content-center content-index">
        <div class="content-index-header cate-header">
            <?php the_title() ?>
        </div>
        <div class="content-index-block">
            <div class="backsg-top">

            </div>
            <div class="backsg-bottom">

            </div>
            <div class="menu-sg-left">
				<div class="menu-sg-header">
					Dịch vụ nổi bật
				</div>
				<ul class="list-art-sg">
					<li>
						<a href="">Nâng mũi S-Line bằng sụn tự thân</a>
					</li>
					<li>
						<a href="">Nâng mũi S-Line bằng sụn tự thân</a>
					</li>
					<li>
						<a href="">Triệt lông vĩnh viễn bằng NANNO LIGHT PRO</a>
					</li>
					<li>
						<a href="">Nâng mũi S-Line bằng sụn tự thân</a>
					</li>
				</ul>
				<div class="menu-sg-header">
					Phẫu thuật thẩm mỹ
				</div>
				<ul class="list-art-sg">
					<li>
						<a href="">Nâng mũi S-Line bằng sụn tự thân</a>
					</li>
					<li>
						<a href="">Nâng mũi S-Line bằng sụn tự thân</a>
					</li>
					<li>
						<a href="">Triệt lông vĩnh viễn bằng NANNO LIGHT PRO</a>
					</li>
					<li>
						<a href="">Nâng mũi S-Line bằng sụn tự thân</a>
					</li>
				</ul>

				<div class="menu-sg-header">
					Thẩm mỹ không phẫu thuật
				</div>
				<ul class="list-art-sg">
					<li>
						<a href="">Nâng mũi S-Line bằng sụn tự thân</a>
					</li>
					<li>
						<a href="">Nâng mũi S-Line bằng sụn tự thân</a>
					</li>
					<li>
						<a href="">Triệt lông vĩnh viễn bằng NANNO LIGHT PRO</a>
					</li>
					<li>
						<a href="">Nâng mũi S-Line bằng sụn tự thân</a>
					</li>
				</ul>

				<div class="menu-sg-header">
					Điều trị và chăm sóc da
				</div>
				<ul class="list-art-sg">
					<li>
						<a href="">Nâng mũi S-Line bằng sụn tự thân</a>
					</li>
					<li>
						<a href="">Nâng mũi S-Line bằng sụn tự thân</a>
					</li>
					<li>
						<a href="">Triệt lông vĩnh viễn bằng NANNO LIGHT PRO</a>
					</li>
					<li>
						<a href="">Nâng mũi S-Line bằng sụn tự thân</a>
					</li>
				</ul>
			</div>
            <!-- Content single -->
            <div class="content-sg-wrapper">
                <h1 class="faq-title">
					Những liệu trình chăm sóc da mặt định kỳ
				</h1>
				<div class="faq-content col-xs-12 none-padding">
					<img src="<?php echo get_template_directory_uri() ?>/assets/images/blog.jpg"/>
					Dưới ảnh hưởng xấu và tác động tiêu cực từ môi trường, da sẽ dần bị tàn phai theo thời gian. Để duy trì làn da luôn khỏe mạnh, cùng với việc bổ sung đầy đủ nguồn dưỡng chất, ngủ nghỉ hợp lý thì việc chăm sóc da định kỳ cũng là việc làm hết sức thiết yếu.

					Những liệu trình chăm sóc da cơ bản tại TMV Anh Thư sẽ là giải pháp giúp bạn giữ gìn làn da chắc khỏe trẻ đẹp hoặc sẽ giúp bạn khôi phục làn da bị tổn thương, nâng cao độ đàn hồi cho da chắc khỏe.

					Liệu trình chăm sóc da bằng Collagen tươi

					Collage tươi giúp cải thiện độ đàn hồi và cải thiện cấu trúc da, làm cho da mượt và 			mịn hơn.
					Sau khi thẩm thấu vào các lớp bên trong của da, collagen tươi kích thích tái tạo tế 			bào, duy trì cân bằng nước và chống lại lão hoá trên da, làm mềm nếp nhăn và làm 			giảm nếp nhăn, cải thiện và nâng cao đường nét trên khuôn mặt.
					Collagen giải quyết hiệu quả làn da bị mất tính đàn hồi do chịu ảnh hưởng của thời 		gian và các yếu tố môi trường.
					Kích thích tái tạo tế bào, giúp trung hoà các tế bào gốc tự do, nuôi dưỡng các lớp tế 		bào sâu hơn của da.
					Collagen tươi còn có tác dụng chống nhăn, nâng và căng cơ mặt, chống lão hoá, ngăn ngừa nếp nhăn, dưỡng sáng mịn da và se nhỏ lỗ chân lông.
				</div>
				<p class="faq-source">(Hà Giang – Nam Định)</p>
				<div class="answer-wrapper col-xs-12 none-padding">
					<p class="answer">Trả lời</p>
					Bạn Hà Giang thân mến!

					Rất cảm ơn bạn đã quan tâm và gửi câu hỏi tới hòm thư tư vấn của thẩm mỹ viện Anh Thư. Chúng tôi xin giải đáp câu hỏi của bạn như sau:

					Các vết thâm sau mụn thông thường là kết quả của việc dùng tay hoặc các vật dụng cứng không sạch sẽ tác động mạnh vào vùng da bị mụn và đang có dấu hiệu viêm nhiễm, khiến da vùng da bên dưới bị tụ máu dẫn tới tình trạng bầm tím và thâm tại. Sẹo thâm là sự tổn thương lớp biểu bì, việc điều trị dễ dàng và đơn giản hơn rất nhiều so với việc điều trị sẹo lõm.

					Đúng như bạn nói ở trên, hiện nay, Thẩm mỹ viện Anh Thư đã ứng dụng thành công phương pháp trị vết thâm bằng sự kết hợp giữa tinh chất Peeling và nguồn ánh sáng E-light. Đặc biệt, công nghệ này đã được FDA (Cục quản lý thực phẩm và dược phẩm Hoa Kỳ) chứng nhận về độ an toàn và hiệu quả của nó tới người sử dụng.
					<p class="faq-source">Bác sĩ tư vấn</p>
					<p class="faq-source">anhthubeauty.com</p>
				</div>
				<div class="contact-sg-wrapper">
					Hãy liên hệ ngay với các chuyên gia nhiệt tình của chúng tôi để bạn có được một buổi khám và tư vấn miễn phí! CALL: <span>0934.111.373</span>
				</div>
				<div class="post-related-wrapper col-xs-12 none-padding">
					<div class="post-related-header">
						Các tin liên quan
					</div>
					<ul>
						<li>
							<a href="">Trị tàn nhang hết bao nhiêu tiền?</a>
						</li>
						<li>
							<a href="">Trị hôi nách bằng phương pháp hút bỏ nội soi có triệt để không?</a>
						</li>
						<li>
							<a href="">Những băn khoăn của khách hàng khi sử dụng dịch vụ Nâng mũi bọc sụn</a>
						</li>
						<li>
							<a href="">Trị tàn nhang hết bao nhiêu tiền?</a>
						</li>
						<li>
							<a href="">Trị tàn nhang hết bao nhiêu tiền?</a>
						</li>
					</ul>
				</div>
				<div class="sv-more-wrapper col-xs-12 none-padding">
					<div class="sv-more-header">
						Các dịch vụ khác
					</div>
					<div class="sv-more-item">
						<a href="">
							<img src="/assets/images/1.jpg">
							<div class="sv-more-itemdes">
								Điều trị mụn
							</div>
						</a>
					</div>
					<div class="sv-more-item">
						<a href="">
							<img src="/assets/images/1.jpg">
							<div class="sv-more-itemdes">
								Điều trị mụn
							</div>
						</a>
					</div>
					<div class="sv-more-item">
						<a href="">
							<img src="/assets/images/1.jpg">
							<div class="sv-more-itemdes">
								Điều trị mụn
							</div>
						</a>
					</div>
					<div class="sv-more-item">
						<a href="">
							<img src="/assets/images/1.jpg">
							<div class="sv-more-itemdes">
								Điều trị mụn
							</div>
						</a>
					</div>
				</div>
				<div class="col-xs-12 none-padding dk-sv-single">
					<div class="dk-service">
						<div class="dk-service-header">
							
						</div>
						<form>
						  <div class="form-group">
						    <label for="exampleInputEmail1">Họ và tên</label>
						    <input type="email" class="form-control" placeholder="">
						  </div>
						  <div class="form-group">
						    <label for="exampleInputPassword1">Số điện thoại</label>
						    <input type="text" class="form-control" placeholder="">
						  </div>
						  <div class="form-group">
						    <label for="exampleInputPassword1">Email</label>
						    <input type="text" class="form-control" placeholder="">
						  </div>
						  <div class="form-group">
						    <label for="exampleInputPassword1">Ghi chú</label>
						    <textarea class="form-control"></textarea>
						  </div>
						  <div class="submit-wrapper">
						  		<button type="submit" class="btn btn-default">Đăng ký</button>
						  </div>
						</form>
					</div>
				</div>
                <?php the_content(); ?>
                <?php get_template_part('content', 'bottom') ?>
                <?php get_template_part('register') ?>
            </div>
            <!-- Menu single right -->
            <?php get_template_part('sidebar', 'right'); ?>
            <div style="clear:both;"></div>
        </div>
    </div>
    <div style="clear:both;"></div>
</div>
<?php get_footer(); ?>