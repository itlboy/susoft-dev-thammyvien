<?php
$introId = 20;
$pttmId = 6;
$tmkptId = 7;
$dtcsdId = 8;
$args = array(
    'posts_per_page' => 5,
    'category' => 20,
);
$introPosts = get_posts($args);
global $footerServiceTerms;
//$serviceTerms = array(6, 7, 8);
?>
<!-- Footer -->
<div class="content-index-wrapper content-footer1-wrapper col-xs-12 none-padding">
    <div class="content-center content-index footer1-block col-xs-6 col-md-6" id="accordion" role="tablist" aria-multiselectable="true">
        <!-- Mobile -->
        <a class="btn btn-collapse content-mobile col-xs-12 none-padding" data-parent="#accordion" role="button" data-toggle="collapse" href="#collapseFooter<?php echo $introId ?>" aria-expanded="true" aria-controls="collapseFooter<?php echo $introId ?>">
	    Giới thiệu
	</a>
        <div class="collapse col-xs-12 none-padding in" id="collapseFooter<?php echo $introId ?>">
	    <div class="col-xs-12 none-padding footer1-item content-mobile">
		<ul>
		    <?php
		    foreach ($introPosts as $post):
			setup_postdata($post);
			?>
    		    <li>
    			<a href="<?php the_permalink() ?>">
				<?php the_title() ?>
    			</a>
    		    </li>
		    <?php endforeach; ?>
		</ul>
	    </div>
        </div>
	<?php
	foreach ($footerServiceTerms as $tax_term):
	    wp_reset_query();
//            $tax_term = get_term($termId, $tax);
//            $posts_array = get_posts(
	    $args = array(
		'posts_per_page' => -1,
		'post_type' => SERVICEPOSTTYPE,
		'post_status' => 'publish',
		'tax_query' => array(
		    array(
			'taxonomy' => SERVICETAX,
			'field' => 'term_id',
			'terms' => $tax_term->term_id,
			'include_children' => false
		    )
		)
	    );
	    $my_query = null;
	    $my_query = new WP_Query($args);
	    ?>
    	<a class="btn btn-collapse content-mobile col-xs-12 none-padding" data-parent="#accordion" role="button" data-toggle="collapse" href="#collapseFooter<?php echo $tax_term->term_id ?>" aria-expanded="true" aria-controls="collapseFooter<?php echo $tax_term->term_id ?>">
		<?php echo $tax_term->name; ?>
    	</a>
	    <?php if ($my_query->have_posts()): ?>
		<div class="collapse col-xs-12 none-padding" id="collapseFooter<?php echo $tax_term->term_id ?>">
		    <div class="col-xs-12 none-padding footer1-item content-mobile">
			<ul>
			    <?php
			    while ($my_query->have_posts()) : $my_query->the_post();
				?>
	    		    <li>
	    			<a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
	    		    </li>
				<?php
			    endwhile;
			    ?>
			</ul>
		    </div>
		</div>
	    <?php endif; ?>
	<?php endforeach; ?>
	<!--        <a class="btn btn-collapse content-mobile col-xs-12 none-padding" role="button" data-toggle="collapse" href="#collapseFooter2" aria-expanded="true" aria-controls="collapseFooter2">
		    Giới thiệu 2
		</a>
		<div class="collapse col-xs-12 none-padding" id="collapseFooter2">
		    <div class="col-xs-12 none-padding footer1-item content-mobile">
			<ul>
	
			    <li>
				<a href="">
				    Giới thiệu
				</a>
			    </li>
			    <li>
				<a href="">
				    Giới thiệu
				</a> 
			    </li>
			    <li>
				<a href="">
				    Giới thiệu
				</a>
			    </li>
			</ul> 
		    </div>
		</div>-->

        <div class="footer1-item content-destop">
            <div class="footer1-title">
                <a href="<?php echo get_category_link($introId) ?>">Giới thiệu</a>
            </div>
            <ul>
		<?php
		foreach ($introPosts as $post):
		    setup_postdata($post);
		    ?>
    		<li>
    		    <a href="<?php the_permalink() ?>">
			    <?php the_title() ?>
    		    </a>
    		</li>
		<?php endforeach; ?>
            </ul>
        </div>
	<?php
	foreach ($footerServiceTerms as $tax_term):
	    wp_reset_query();
//            $tax_term = get_term($termId, $tax);
//            $posts_array = get_posts(
	    $args = array(
		'posts_per_page' => -1,
		'post_type' => SERVICEPOSTTYPE,
		'post_status' => 'publish',
		'tax_query' => array(
		    array(
			'taxonomy' => SERVICETAX,
			'field' => 'term_id',
			'terms' => $tax_term->term_id,
			'include_children' => false
		    )
		)
	    );
	    $my_query = null;
	    $my_query = new WP_Query($args);
	    ?>
    	<div class="footer1-item content-destop">
    	    <div class="footer1-title">
    		<a href="<?php echo get_term_link($tax_term) ?>" >
			<?php echo $tax_term->name; ?>
    		</a>
    	    </div>
		<?php if ($my_query->have_posts()): ?>
		    <ul>
			<?php
			while ($my_query->have_posts()) : $my_query->the_post();
			    ?>
	    		<li>
	    		    <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
	    		</li>
			    <?php
			endwhile;
			?>
		    </ul>
		<?php endif; ?>
    	</div>
	<?php endforeach; ?>
    </div>
    <div class="map-wrapper col-xs-6">
		<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3919.6730302342003!2d106.689675!3d10.759663!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xdcaea22233cb0914!2zMzU5IFRy4bqnbiBIxrBuZyDEkOG6oW8!5e0!3m2!1sen!2sus!4v1445674687706" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
		<!--Google map-->
	    </div>
	    <a class="view-map" href="https://maps.google.com/maps?ll=10.760942,106.68875&z=15&t=m&hl=en-US&gl=US&mapclient=embed">Xem bản đồ lớn hơn</a>
</div>

<!-- Footer 2 -->
<div class="content-index-wrapper content-footer2-wrapper col-xs-12 none-padding">
    <div class="content-center content-index footer2-content">
        <div class="logo-footer content-destop">
            <a href="">
                <img src="<?php echo get_template_directory_uri() ?>/assets/images/logo-footer.png">
            </a>
        </div>
        <div class="fan-facebook col-xs-12 none-padding">
            <div class="fb-page" data-href="<?php the_field('facebook_fanpage', 'option'); ?>" data-width="" data-height="150" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="<?php the_field('facebook_fanpage', 'option'); ?>"><a href="<?php the_field('facebook_fanpage', 'option'); ?>">Thẩm Mỹ Viện Anh Thư</a></blockquote></div></div>
        </div>
        <div class="time-work-wrapper">
            <div class="time-work">
                <p>Thời gian làm việc</p>
                <p>
                    Buổi sáng:<span> 08:00 - 12:00</span>
                </p>
                <p>
                    Buổi chiều:<span> 13:00 - 21:00</span>
                </p>
                <span>(Kể cả ngày lễ và chủ nhật)</span>
            </div>
        </div>

        <div class="social-footer social-wrapper content-destop">
            <a href="<?php the_field('facebook_fanpage', 'option'); ?>">
                <span class="facebook"></span>
            </a>
            <a href="<?php the_field('twitter_link', 'option'); ?>">
                <span class="twitter"></span>
            </a>
            <a href="<?php the_field('youtube_link', 'option'); ?>">
                <span class="youtube"></span>
            </a>
            <a href="<?php the_field('istagram_link', 'option'); ?>">
                <span class="istagram"></span>
            </a>
        </div>
        <div class="time-work-wrapper">

	    <div class="subscribe-wrapper ">
		<form id="registerform" action="">
		    <input name="email" class="text-input" type="email" placeholder="Email của bạn" />
		    <input class="submit-input" type="submit" name="submit" value="Đăng ký nhận thông tin" />
		</form>
	    </div>
	    <div class="website-infor">
		<p>TMV Anh Thư | 373 Trần Hưng Đạo, P. Cầu Kho, Quận 1, TP.HCM</p>
		<p>Hotline: (08) 3920.2345 – (08) 3920.6789 – 0934111373 </p>
		<p>info@anhthubeauty.com | www.anhthubeauty.com</p>
	    </div>
	    <p class="copy-right">© Copyright 2015 anhthubeauty.com, All rights reserved.</p>
        </div>
    </div>
    <div class="contact-wrapper content-mobile contact-footer">
        <a class="contact-home" href="<?php echo bloginfo('home') ?>"></a>
        <a class="contact-km" href="<?php echo get_category_link(16) ?>"></a>
        <a class="contact-tv" href=""  data-toggle="modal" data-target="#myModal"></a>
        <a class="contact-facebook" href="<?php the_field('facebook_fanpage', 'option'); ?>"></a>
        <a class="contact-phone" href=""></a>
    </div>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-tv-wrapper" role="document">
        <div class="modal-content modal-tv ">
            <form id="help-form2" action="<?php echo admin_url("admin-ajax.php"); ?>"  method="POST" class="help-form">
                <div class="form-group">
                    <label for="exampleInputEmail1">Họ và tên</label>
                    <input  type="text" class="form-control name" placeholder="">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Số điện thoại</label>
                    <input type="text" class="form-control mobile" placeholder="">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Email</label>
                    <input type="email" class="form-control email" placeholder="">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Ghi chú</label>
                    <textarea class="form-control note"></textarea>
                </div>
                <button type="submit" class="btn btn-default submit-dk sv-more-header">Gửi câu hỏi</button>
                <p class="note-dk">
                    *Câu hỏi của bạn để được các Bác sĩ Thẩm mỹ viện Anh Thư tư vấn. Hoặc gọi tới số <span>1900 6466</span> để được tư vấn trực tiếp.
                </p>
            </form>
        </div>
    </div>
</div>
<?php include "tawkScript.php"; ?>
<?php wp_footer(); ?>
</body>
</html>