<?php

ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);
date_default_timezone_set('Asia/Ho_Chi_Minh');

// opcache_reset();
function includeDir($dir, $depth = 2) {
    if ($depth == 0) {
        return;
    }

    // require all php files
    $scan = glob("$dir/*");
    foreach ($scan as $path) {
        if (preg_match('/\.php$/', $path)) {
            require_once $path;
        } elseif (is_dir($path)) {
            includeDir($path, $depth--);
        }
    }
}

//add_action('admin_init', function() {
includeDir(__DIR__ . '/include', 10);
//});
spl_autoload_register(function($className) {
    if (file_exists(__DIR__ . DIRECTORY_SEPARATOR . 'auto-load' . DIRECTORY_SEPARATOR . $className . '.php'))
        include __DIR__ . DIRECTORY_SEPARATOR . 'auto-load' . DIRECTORY_SEPARATOR . $className . '.php';
});

function pageTitle() {
    echo get_bloginfo('name');
    wp_title('-');
}

add_action('init', function () {
    register_nav_menu('topMenu', __('Top menu'));
});
add_action('init', function() {
    register_nav_menu('headerLeft', __('Header left menu'));
});
add_action('init', function() {
    register_nav_menu('headerRight', __('Header right menu'));
});
add_theme_support('post-thumbnails');
// add service postype
//Add custom postype

add_action('init', 'create_post_type');

function create_post_type() {
    register_post_type('dichvu', array(
        'labels' => array(
            'name' => _('Các dịch vụ'),
            'singular_name' => 'dichvu',
            'add_new' => _('Thêm mới'),
            'add_new_item' => __('Thêm dịch vụ mới'),
            'edit_item' => __('Sửa dịch vụ'),
            'new_item' => __('Dịch vụ mới'),
            'all_items' => __('Tất cả dịch vụ'),
            'view_item' => __('Xem dịch vụ'),
            'search_items' => __('Tìm dịch vụ'),
            'not_found' => __('Không tìm thấy dịch vụ nào'),
            'not_found_in_trash' => __('Không tìm thấy dịch vụ nào trong thùng rác'),
            'parent_item_colon' => '',
            'menu_name' => 'Dịch vụ'
        ),
        'public' => true,
        'has_archive' => true,
        'supports' => array('thumbnail', 'content', 'editor', 'title'),
            )
    );
}

add_action('init', 'create_bacsituvan');

function create_bacsituvan() {
    register_post_type('bacsituvan', array(
        'labels' => array(
            'name' => _('Các bài viết'),
            'singular_name' => 'bacsituvan',
            'add_new' => _('Thêm mới'),
            'add_new_item' => __('Thêm bài viết mới'),
            'edit_item' => __('Sửa bài viết'),
            'new_item' => __('Bài viết mới'),
            'all_items' => __('Tất cả bài viết'),
            'view_item' => __('Xem bài viết'),
            'search_items' => __('Tìm bài viết'),
            'not_found' => __('Không tìm thấy bài viết nào'),
            'not_found_in_trash' => __('Không tìm thấy bài viết nào trong thùng rác'),
            'parent_item_colon' => '',
            'menu_name' => 'Bác sĩ tư vấn'
        ),
        'public' => true,
        'has_archive' => true,
        'supports' => array('thumbnail', 'content', 'editor', 'title'),)
    );
}

function themes_taxonomy() {
    register_taxonomy(
            'danhmuc-dichvu', //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces). 
            'dichvu', //post type name
            array(
        'hierarchical' => true,
        'label' => 'Danh mục', //Display name
        'query_var' => true,
        'rewrite' => array(
            'slug' => 'danhmuc-dichvu', // This controls the base slug that will display before each term
            'with_front' => false // Don't display the category base before 
        ),)
    );
}

add_action('init', 'themes_taxonomy');

// Settings page
if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'Cài đặt trang',
        'menu_title' => 'Cài đặt trang',
        'menu_slug' => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect' => false
    ));

    acf_add_options_sub_page(array(
        'page_title' => 'Cài đặt mạng xa hội',
        'menu_title' => 'Cài đặt mạng xa hội',
        'parent_slug' => 'theme-general-settings',
    ));
    acf_add_options_sub_page(array(
        'page_title' => 'Album ảnh',
        'menu_title' => 'Album ảnh',
        'parent_slug' => 'theme-general-settings',
    ));
}

// nav
function page_nav() {

    if (is_singular())
        return;

    global $wp_query;

    /** Stop execution if there's only 1 page */
    if ($wp_query->max_num_pages <= 1)
        return;

    $paged = get_query_var('paged') ? absint(get_query_var('paged')) : 1;
    $max = intval($wp_query->max_num_pages);

    /** Add current page to the array */
    if ($paged >= 1)
        $links[] = $paged;

    /** Add the pages around the current page to the array */
    if ($paged >= 3) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if (( $paged + 2 ) <= $max) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<div class="panigation-wrapper"><ul>';

    /** Previous Post Link */
    if (get_previous_posts_link('«'))
        printf('<li>%s</li>', get_previous_posts_link('«'));

    /** Link to first page, plus ellipses if necessary */
    if (!in_array(1, $links)) {
        $class = 1 == $paged ? ' class="active"' : '';

        printf('<li%s><a href="%s">%s</a></li>', $class, esc_url(get_pagenum_link(1)), '1');

        if (!in_array(2, $links))
            echo '<li>…</li>';
    }

    /** Link to current page, plus 2 pages in either direction if necessary */
    sort($links);
    foreach ((array) $links as $link) {
        $class = $paged == $link ? ' class="active"' : '';
        printf('<li%s><a href="%s">%s</a></li>', $class, esc_url(get_pagenum_link($link)), $link);
    }

    /** Link to last page, plus ellipses if necessary */
    if (!in_array($max, $links)) {
        if (!in_array($max - 1, $links))
            echo '<li>…</li>';

        $class = $paged == $max ? ' class="active"' : '';
        printf('<li%s><a href="%s">%s</a></li>', $class, esc_url(get_pagenum_link($max)), $max);
    }

    /** Next Post Link */
    if (get_next_posts_link('»'))
        printf('<li>%s</li>', get_next_posts_link('»'));

    echo '</ul></div>';
}

function excerpt($limit) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt) >= $limit) {
        array_pop($excerpt);
        $excerpt = implode(" ", $excerpt) . '...';
    } else {
        $excerpt = implode(" ", $excerpt);
    }
    $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);
    return $excerpt;
}

function content($limit) {
    $content = explode(' ', get_the_content(), $limit);
    if (count($content) >= $limit) {
        array_pop($content);
        $content = implode(" ", $content) . '...';
    } else {
        $content = implode(" ", $content);
    }
    $content = preg_replace('/\[.+\]/', '', $content);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    return $content;
}

function cutStr($str, $limit) {
    $content = explode(' ', $str, $limit);
    if (count($content) >= $limit) {
        array_pop($content);
        $content = implode(" ", $content) . '...';
    } else {
        $content = implode(" ", $content);
    }
    $content = preg_replace('/\[.+\]/', '', $content);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    return $content;
}

add_action('init', 'create_cauhoituvan');

function create_cauhoituvan() {
    register_post_type('cauhoituvan', array(
        'labels' => array(
            'name' => _('Câu hỏi tư vấn'),
            'singular_name' => 'cauhoituvan',
//            'add_new' => _('Thêm mới'),
//            'add_new_item' => __('Thêm câu hỏi mới'),
//            'edit_item' => __('Sửa bài viết'),
            'new_item' => __('Câu hỏi mới'),
            'all_items' => __('Tất cả câu hỏi'),
            'view_item' => __('Xem câu hỏi'),
            'search_items' => __('Tìm câu hỏi'),
            'not_found' => __('Không tìm thấy câu hỏi nào'),
            'not_found_in_trash' => __('Không tìm thấy câu hỏi nào trong thùng rác'),
            'parent_item_colon' => '',
            'menu_name' => 'Câu hỏi tư vấn',
        ),
        'public' => true,
//        'rewrite' => false,
//        'publicly_queryable' => true,
        'has_archive' => true,
        'supports' => array('content', 'editor', 'title'),
        'capabilities' => array(
            'create_posts' => false, // Removes support for the "Add New" function ( use 'do_not_allow' instead of false for multisite set ups )
        ),
        'map_meta_cap' => true,
            )
    );
}

add_action('init', 'create_dangkynhanthongtin');

function create_dangkynhanthongtin() {
    register_post_type('dangkynhanthongtin', array(
        'labels' => array(
            'name' => _('Đăng ký nhận thông tin'),
            'singular_name' => 'cauhoituvan',
//            'add_new' => _('Thêm mới'),
//            'add_new_item' => __('Thêm câu hỏi mới'),
//            'edit_item' => __('Sửa bài viết'),
//            'new_item' => __('Câu hỏi mới'),
            'all_items' => __('Tất cả đăng ký'),
            'view_item' => __('Xem đăng ký'),
            'search_items' => __('Tìm đăng ký'),
            'not_found' => __('Không tìm thấy đăng ký nào'),
            'not_found_in_trash' => __('Không tìm thấy đăng ký nào trong thùng rác'),
            'parent_item_colon' => '',
            'menu_name' => 'Đăng ký nhận thông tin',
        ),
        'public' => true,
//        'rewrite' => false,
//        'publicly_queryable' => true,
        'has_archive' => true,
        'supports' => array('content', 'editor', 'title'),
        'capabilities' => array(
            'create_posts' => false, // Removes support for the "Add New" function ( use 'do_not_allow' instead of false for multisite set ups )
        ),
        'map_meta_cap' => false,
            )
    );
}

HelpForm::run();
Subscriber::run();
