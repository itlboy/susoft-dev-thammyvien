<!-- Content single -->
<div class="content-sg-wrapper sg-page-wrapper">
    <div class="slide-page-wrapper col-xs-12 none-padding">
        <div id="carousel-sgpage-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators carousel-bannertop">
                <li data-target="#carousel-sgpage-generic" data-slide-to="0" class="active">1</li>
                <li data-target="#carousel-sgpage-generic" data-slide-to="1">2</li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/banner.png" alt="...">
                </div>
                <div class="item">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/banner.png" alt="...">
                </div>
            </div>
        </div>
    </div>
    <h1 class="art-sg-title">
        Những liệu trình chăm sóc da mặt định kỳ
    </h1>
    Dưới ảnh hưởng xấu và tác động tiêu cực từ môi trường, da sẽ dần bị tàn phai theo thời gian. Để duy trì làn da luôn khỏe mạnh, cùng với việc bổ sung đầy đủ nguồn dưỡng chất, ngủ nghỉ hợp lý thì việc chăm sóc da định kỳ cũng là việc làm hết sức thiết yếu.

    Những liệu trình chăm sóc da cơ bản tại TMV Anh Thư sẽ là giải pháp giúp bạn giữ gìn làn da chắc khỏe trẻ đẹp hoặc sẽ giúp bạn khôi phục làn da bị tổn thương, nâng cao độ đàn hồi cho da chắc khỏe.

    Liệu trình chăm sóc da bằng Collagen tươi

    Collage tươi giúp cải thiện độ đàn hồi và cải thiện cấu trúc da, làm cho da mượt và 			mịn hơn.
    Sau khi thẩm thấu vào các lớp bên trong của da, collagen tươi kích thích tái tạo tế 			bào, duy trì cân bằng nước và chống lại lão hoá trên da, làm mềm nếp nhăn và làm 			giảm nếp nhăn, cải thiện và nâng cao đường nét trên khuôn mặt.
    Collagen giải quyết hiệu quả làn da bị mất tính đàn hồi do chịu ảnh hưởng của thời 		gian và các yếu tố môi trường.
    Kích thích tái tạo tế bào, giúp trung hoà các tế bào gốc tự do, nuôi dưỡng các lớp tế 		bào sâu hơn của da.
    Collagen tươi còn có tác dụng chống nhăn, nâng và căng cơ mặt, chống lão hoá, ngăn ngừa nếp nhăn, dưỡng sáng mịn da và se nhỏ lỗ chân lông.
    <div class="sg-page-testi">
        <h2>
            Khách hàng của chúng tôi
        </h2>
        <div class="progress">
            <div class="progress-bar bold-progress" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%;">
                Phẫu thuật thẩm mỹ 90%
            </div>
        </div>
        <div class="progress light-progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="81" aria-valuemin="0" aria-valuemax="100" style="width: 81%;">
                Chăm sóc da cơ bản 81%
            </div>
        </div>
        <div class="progress">
            <div class="progress-bar bold-progress" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%;">
                Điều trị Mụn 90%
            </div>
        </div>
        <div class="progress light-progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="81" aria-valuemin="0" aria-valuemax="100" style="width: 82%;">
                Điều trị nám 82%
            </div>
        </div>
        <div class="progress light-progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="81" aria-valuemin="0" aria-valuemax="100" style="width: 85%;">
                Giảm cân 85%
            </div>
        </div>
        <div class="progress light-progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%;">
                Triệt lông vĩnh viễn 75%
            </div>
        </div>
        <div class="progress">
            <div class="progress-bar bold-progress" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 88%;">
                Tắm trắng da 88%
            </div>
        </div>
        <div class="dt-wrapper">
            <img src="<?php echo get_template_directory_uri() ?>/assets/images/1.jpg">
            <h2>Tiến sĩ - Bác sĩ Nguyễn Anh Tuấn</h2>
            <p class="caption">Trưởng khoa Tạo Hình Thẩm Mỹ</p>
            <p class="caption">Bệnh Viện Đại Học Y Dược TP. Hồ Chí Minh</p>
            <div class="dt-content">
                Với gần 30 năm kinh nghiệm, trong đó 20 năm là phẫu thuật viên Chấn thương Chỉnh hình và gần 10 năm là phẫu thuật viên thẩm mỹ, bác sĩ Nguyễn Anh Tuấn từng theo học chuyên sâu ngành Tạo hình – Thẩm mỹ tại các nước tiên tiến như Đức, Hoa Kỳ… và được mệnh danh là đôi bàn tay vàng trong ngành Tạo hình – Thẩm mỹ Việt Nam với những ca phẫu thuật an toàn, hiệu quả làm đẹp rõ rệt và mang lại sự hài lòng cho khách hàng. Tại AnhthuBeauty, Tiến sĩ – bác sĩ Nguyễn Anh Tuấn đảm nhận vai trò phẫu thuật viên chính cho tất cả các ca phẫu thuật thẩm mỹ. Phương châm của ông là mang lại vẻ đẹp tự nhiên, hài hòa và phù hợp nhất với cá tính riêng của mỗi khách hàng, giúp họ tự tin và hạnh phúc hơn với vẻ bề ngoài của mình.
            </div>
        </div>

    </div>
    <div class="contact-sg-wrapper">
        Hãy liên hệ ngay với các chuyên gia nhiệt tình của chúng tôi để bạn có được một buổi khám và tư vấn miễn phí! CALL: <span>0934.111.373</span>
    </div>
    <div class="sv-more-wrapper">
        <div class="sv-more-header">
            Các dịch vụ khác
        </div>
        <div class="sv-more-item">
            <a href="">
                <img src="/assets/images/1.jpg">
                <div class="sv-more-itemdes">
                    Điều trị mụn
                </div>
            </a>
        </div>
        <div class="sv-more-item">
            <a href="">
                <img src="/assets/images/1.jpg">
                <div class="sv-more-itemdes">
                    Điều trị mụn
                </div>
            </a>
        </div>
        <div class="sv-more-item">
            <a href="">
                <img src="/assets/images/1.jpg">
                <div class="sv-more-itemdes">
                    Điều trị mụn
                </div>
            </a>
        </div>
        <div class="sv-more-item">
            <a href="">
                <img src="/assets/images/1.jpg">
                <div class="sv-more-itemdes">
                    Điều trị mụn
                </div>
            </a>
        </div>
    </div>
    <div class="col-xs-12 none-padding dk-sv-single">
        <div class="dk-service">
            <div class="dk-service-header">

            </div>
            <form>
                <div class="form-group">
                    <label for="exampleInputEmail1">Họ và tên</label>
                    <input type="email" class="form-control" placeholder="">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Số điện thoại</label>
                    <input type="text" class="form-control" placeholder="">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Email</label>
                    <input type="text" class="form-control" placeholder="">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Ghi chú</label>
                    <textarea class="form-control"></textarea>
                </div>
                <div class="submit-wrapper">
                    <button type="submit" class="btn btn-default">Đăng ký</button>
                </div>
            </form>
        </div>
    </div>

</div>