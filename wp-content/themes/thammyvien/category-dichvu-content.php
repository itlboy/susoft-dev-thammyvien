<!-- Content cat -->
<div class="content-index-wrapper col-xs-12 none-padding category-content">
    <div class="content-center content-index">
        <div class="content-index-header cate-header">
            <?php single_cat_title('', true); ?>
        </div>
        <?php
        wp_reset_query();
        if (have_posts()):
            ?>
            <div class="content-index-block">
                <?php
                while (have_posts()):
                    the_post();
                    ?>
                    <div class="cate-item-wrapper">
                        <div class="content-index-item cate-item">
                            <a href="<?php the_permalink() ?>">
                                <?php customThumb(217,294) ?>
                            </a>
                        </div>
                        <div class="content-item-title">
                            <a href="<?php the_permalink() ?>">
                                <?php the_title() ?>
                            </a>
                        </div>
                        <?php
                        $timeToService = get_field('time');
                        if ($timeToService):
                            ?>
                            <p><?php echo $timeToService ?> phút</p>
                        <?php endif; ?>
                        <p><?php echo excerpt(30); ?></p>
                        <a class="viewmore-cat" href="<?php the_permalink() ?>">Xem tiếp...</a>
                    </div>

                <?php endwhile; ?>
                <div style="clear:both;"></div>
            </div>
        <?php endif; ?>
    </div>
    <div style="clear:both;"></div>
</div>