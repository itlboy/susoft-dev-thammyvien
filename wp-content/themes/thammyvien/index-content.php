<?php
$categoryId = 12;
$args = array(
    'posts_per_page' => 11,
    'category' => $categoryId,
);
$posts = get_posts($args);
?> 
<!-- Content index -->
<div class="content-index-wrapper col-xs-12 none-padding">
    <div class="content-center content-index">
        <a href="<?php echo get_category_link($categoryId) ?>">
            <div class="content-index-header">
                <?php echo get_the_category_by_ID($categoryId) ?>
            </div>
        </a>
        <div class="content-index-block content-destop">
            <?php
            global $post;
            $i = 0;
            foreach ($posts as $post):
                setup_postdata($post);
                $i++;
                ?>
                <div class="content-index-item">
                    <a href="<?php the_permalink(); ?>">
                        <?php customThumb(198, 198); ?>
                        <?php // the_post_thumbnail(array(198,198)) ?>
                        <div class="content-item-title">
                            <?php the_title(); ?>
                        </div>
                    </a>
                </div>
                <?php
                if ($i == 4)
                    break;
            endforeach;
            ?>
            <!-- blog index -->
            <div class="blog-wrapper col-xs-12 none-padding">
                <?php
                if (isset($posts[4])):
                    $post = $posts[4];
                    setup_postdata($post);
                    ?>
                    <div class="blog-left col-xs-5 none-padding">
                        <a href="<?php the_permalink() ?>">
                            <?php customThumb(450, 360); ?>
                            <?php // the_post_thumbnail() ?>
                        </a>
                        <div class="blog-info">
                            <a href="<?php the_permalink() ?>">
                                <p class="blog-title">
                                    <?php the_title() ?>
                                </p>
                            </a>
                            <div class="blog-des">
                                <?php the_excerpt(); ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="blog-right col-xs-7 none-padding">
                    <?php
                    for ($i = 5; $i <= 10; $i++):

                        if (isset($posts[$i])) {
                            $post = $posts[$i];
                            setup_postdata($post);
                        } else {
                            break;
                        }
                        ?>
                        <div class="col-xs-6 none-padding blog-ritem">
                            <div class="blog-thumbnail">
                                <a href="<?php the_permalink() ?>">
                                    <?php customThumb(130, 104); ?>
                                    <?php // the_post_thumbnail() ?>
                                </a>
                            </div>
                            <div class="blog-ritem-title">
                                <a href="<?php the_permalink() ?>">
                                    <?php the_title(); ?>
                                </a>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>
            </div>
            <div style="clear:both;"></div>
        </div>
        <div class="content-index-block content-sv-index content-mobile">
            <div id="carousel-svmobile-generic" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <?php
                        global $post;
                        $i = 0;
                        foreach ($posts as $post):
                            $i++;
                            setup_postdata($post);
                            ?>
                            <div class="content-index-item">
                                <a href="<?php the_permalink() ?>">
                                    <?php customThumb(198, 198); ?>
                                    <?php // the_post_thumbnail() ?>
                                    <div class="content-item-title">
                                        <?php the_title() ?>
                                    </div>
                                </a>
                            </div>
                            <?php
                            if ($i == 2)
                                break;
                        endforeach;
                        ?>
                    </div>
                    <div class="item">
                        <?php
                        global $post;
                        for ($i = 2; $i > 0; $i++):
                            if (!isset($posts[$i]))
                                break;
                            else {
                                $post = $posts[$i];
                                setup_postdata($post);
                            }
                            ?>
                            <div class="content-index-item">
                                <a href="<?php the_permalink() ?>">
                                    <?php customThumb(198, 198); ?>
                                    <?php // the_post_thumbnail() ?>
                                    <div class="content-item-title">
                                        <?php the_title() ?>
                                    </div>
                                </a>
                            </div>
                            <?php
                            if ($i == 3)
                                break;
                        endfor;
                        ?>
                    </div>
                </div>
                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-svmobile-generic" role="button" data-slide="prev">
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-svmobile-generic" role="button" data-slide="next">
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
    <div style="clear:both;"></div>
</div>
<?php
wp_reset_postdata();
?>