<?php get_header() ?>
<!-- Content cat -->
<?php the_post(); ?>
<div class="content-index-wrapper col-xs-12 none-padding sg-content">
    <div class="content-center content-index">
        <div class="content-index-header cate-header">
            <?php echo cutStr(get_the_title(), 8) ?> 
        </div>
        <div class="content-index-block">
            <?php get_template_part('sidebar', 'left') ?>
            <!-- Content single -->
            <div class="content-sg-wrapper">
                <?php if (has_post_thumbnail()): ?>
                    <div class="sg-thmbnail">
                        <!--<img src="/assets/images/1.jpg">-->
                        <?php customThumb(293, 214) ?>
                    </div>
                <?php endif; ?>
                <?php the_content(); ?>
                <?php get_template_part('content', 'bottom') ?>
                <?php get_template_part('register') ?>
            </div>
            <!-- Menu single right -->
            <?php get_template_part('sidebar', 'right'); ?>
            <div style="clear:both;"></div>
        </div>
    </div>
    <div style="clear:both;"></div>
</div>
<?php get_footer() ?>