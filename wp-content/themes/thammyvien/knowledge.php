<?php
$categoryId = 18;
$numberPost = 4;
$args = array(
    'posts_per_page' => $numberPost,
    'category' => $categoryId,
);
$posts = get_posts($args);
?>
<!-- Kien thuc lam dep -->
<div class="content-index-wrapper content-kt-wrapper col-xs-12 none-padding">
    <div class="content-center content-index kt-block">

        <a href="<?php echo get_category_link($categoryId) ?>"> 
            <div class="kt-block-header">
            </div> 
        </a>
        <?php
        global $post;
        $post = $posts[0];
        setup_postdata($post);
        ?>
        <div class="thumbnail-larg">
            <a href="<?php the_permalink() ?>">
                <?php customThumb(400,323) ?>
            </a>
            <div class="blog-info">
                <a href="<?php the_permalink() ?>">
                    <p class="blog-title">
                        <?php the_title() ?>
                    </p>
                </a>
                <div class="blog-des">
                    <?php excerpt(50); ?>
                </div>
            </div>
        </div>
        <div class="thumbnail-small">
            <?php
            for ($i = 1; $i <= 3; $i++):
                if (isset($posts[$i])) {
                    $post = $posts[$i];
                    setup_postdata($post);
                } else {
                    break;
                }
                ?>
                <div class="thumbnail-small-item">
                    <a href="<?php the_permalink() ?>">
                         <?php customThumb(136,100) ?>
                    </a>
                    <div class="thumbnail-small-info">
                        <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
                        <p>
                            <?php echo excerpt(20); ?>
                        </p>
                    </div>
                </div>
            <?php endfor; ?>
        </div>
    </div>
</div>
<?php wp_reset_postdata(); ?>