<?php

global $leftSideBarTerms, $bottomTerms,
 $featurePosts, $newPosts,
 $supportPosts, $footerServiceTerms,
 $currentUrlPath;
$currentUrlPath = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$leftSideBarTerms = array();
$footerServiceTerms = array();
// no default values. using these as examples
define('SERVICETAX', 'danhmuc-dichvu');
define('SERVICEPOSTTYPE', 'dichvu');
$taxonomies = array(
    SERVICETAX
);

$args = array(
    'orderby' => 'name',
    'order' => 'ASC',
    'hide_empty' => false,
    'exclude' => array(),
    'exclude_tree' => array(),
    'include' => array(),
    'number' => '',
    'fields' => 'all',
    'slug' => '',
    'parent' => '',
    'hierarchical' => true,
//    'child_of' => 0,
    'childless' => false,
    'get' => '',
    'name__like' => '',
    'description__like' => '',
    'pad_counts' => false,
    'offset' => '',
    'search' => '',
    'cache_domain' => 'core'
);
$terms = get_terms($taxonomies, $args);
foreach ($terms as $term) {
    if (get_field('left-sidebar', $term)) {
        array_push($leftSideBarTerms, $term);
    }
    if (get_field('add_to_footer', $term)) {
        array_push($footerServiceTerms, $term);
    }
}
// Get feature posts
wp_reset_query();
$args = array(
    'numberposts' => -1,
    'meta_key' => 'feature_post',
    'meta_value' => '1'
);
$featurePosts = get_posts($args);
//wp_reset_query();
// Get new posts
$args = array(
    'numberposts' => 5,
);
$newPosts = get_posts($args);

// Get new posts
$args = array(
    'post_type' => 'bacsituvan',
    'caller_get_posts' => 1
);
$supportPosts = get_posts($args);
